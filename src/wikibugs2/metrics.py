# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import functools
import time

import aioprometheus
import aioprometheus.asgi.quart

REQUESTS_IN_PROGRESS = aioprometheus.Gauge(
    "requests_in_progress",
    "Number of requests in progress",
)

EVENTS_SENT = aioprometheus.Counter(
    "events_sent",
    "Number of events sent",
)

LAST_SEEN_TIMES = {}

BOOT_TIME = time.time()

# Decorator for tracking requests_in_progress.
inprogress = functools.partial(aioprometheus.inprogress, REQUESTS_IN_PROGRESS)


def add_middleware(app):
    """Wrap a Quart app in Prometheus middleware and expose /metrics."""
    app.asgi_app = aioprometheus.MetricsMiddleware(
        app=app.asgi_app,
        exclude_paths=aioprometheus.asgi.middleware.EXCLUDE_PATHS
        + ("/healthz",),
    )
    app.add_url_rule(
        "/metrics",
        "metrics",
        aioprometheus.asgi.quart.metrics,
        methods=["GET"],
    )
    return app


def record_last_seen(thing):
    """Record a unix timestamp in metrics.LAST_SEEN_TIMES."""
    LAST_SEEN_TIMES[thing] = time.time()
