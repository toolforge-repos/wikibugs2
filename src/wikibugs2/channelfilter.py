# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
import collections
import hashlib
import logging
import os
import re

import yaml

from . import utils

logger = logging.getLogger(__name__)


class ChannelFilter:

    @classmethod
    async def create(cls, origin):
        self = cls()
        self.origin = origin
        self.last_load_time = 0
        self.mtime = 0
        self.config = {}
        await self._load()
        return self

    @property
    def origin_is_http(self):
        return str(self.origin).startswith("http")

    async def _load(self, new_config=None):
        if new_config is None:
            if self.origin_is_http:
                self.config = await self._read_from_url()
            else:
                self.config = self._read_from_disk()
        else:
            self.config = new_config
        self._compile_channels()
        loop = asyncio.get_running_loop()
        self.last_load_time = loop.time()
        logger.info("SHA256sum: %s", self.sha256sum)
        logger.info("Channels: %s", self.all_channels())

    async def _read_from_url(self):
        logger.info("Reading config from %s", self.origin)
        async with utils.create_aiohttp_client_session() as session:
            async with session.get(self.origin) as resp:
                resp.raise_for_status()
                return await resp.json()

    def _read_from_disk(self):
        logger.info("Reading config from %s", self.origin)
        self.mtime = os.path.getmtime(self.origin)
        with open(self.origin, encoding="utf-8") as f:
            config = yaml.safe_load(f)
            config["sha256sum"] = hashlib.sha256(
                f.read().encode("utf-8"),
                usedforsecurity=False,
            ).hexdigest()
            return config

    def _compile_channels(self):
        """Compile configuration into more optimal runtime structure."""
        checks = collections.defaultdict(dict)
        for channel, patterns in self.config["channels"].items():
            for pattern, filters in patterns.items():
                matcher = re.compile(
                    pattern,
                    flags=re.IGNORECASE | re.UNICODE,
                )
                checks[channel][matcher] = filters
        self.config["channels"] = checks

    @property
    def firehose_channel(self):
        return self.config["firehose_channel"]

    @property
    def default_channel(self):
        return self.config["default_channel"]

    @property
    def sha256sum(self):
        return self.config["sha256sum"]

    def all_channels(self):
        channels = {
            self.default_channel,
            self.firehose_channel,
        }
        channels.update(self.config["channels"].keys())
        return [c for c in channels if c.startswith("#")]

    async def update(self):
        loop = asyncio.get_running_loop()
        if loop.time() - self.last_load_time < 60:
            # Do not update more often than once a minute
            return False

        if self.origin_is_http:
            new_config = await self._read_from_url()
            if new_config["sha256sum"] == self.sha256sum:
                # Config hasn't changed
                return False
            await self._load(new_config)
        else:
            mtime = os.path.getmtime(self.origin)
            if mtime == self.mtime:
                return False
            await self._load()
        return self.sha256sum

    def channels_for(self, inputs, selector=None):
        """Get all channels that should be notified about the inputs.

        :param inputs: Inputs to check
        :type inputs: dict[label: attributes]
        :param selector: Selector function to test matching inputs
        :type selector: callable
        :returns: dict[channel: list(labels)]
        """
        channels = collections.defaultdict(list)
        for channel, patterns in self.config["channels"].items():
            for matcher, filters in patterns.items():
                for label, attributes in inputs.items():
                    if matcher.fullmatch(label):
                        if filters is None:
                            channels[channel].append(label)
                        elif selector is None:
                            channels[channel].push(label)
                        elif selector(label, attributes, filters):
                            channels[channel].append(label)

        if not channels:
            channels[self.default_channel] = []
        channels.pop("/dev/null", None)
        channels[self.firehose_channel] = []

        return channels


def branch_selector(label, attributes, filters):  # noqa: U100 Unused argument
    """Selector function for use with ChannelFilter.

    :param label: Repository name
    :param attributes: {"branch": "branch name"} data
    :param filters: None or {"branch": "regex pattern"} config
    :returns: boolean match status
    """
    if filters:
        if "branch" in filters:
            m = re.match(filters["branch"], attributes["branch"])
            return m is not None
    return True
