# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
import logging
import logging.handlers
import os

import hypercorn.logging


def private_open(file, flags, dir_fd=None):
    """Open a file with ``u=rw,g=,o=`` permissions."""
    return os.open(file, flags, mode=0o600, dir_fd=dir_fd)


class PrivateTimedRotatingFileHandler(
    logging.handlers.TimedRotatingFileHandler,
):
    """Rotate log files at timed intervals with ``u=rw,g=,o=`` permissions."""

    def _open(self):
        return open(
            self.baseFilename,
            self.mode,
            encoding=self.encoding,
            opener=private_open,
        )


class HypercornLogger(hypercorn.logging.Logger):
    """Configure logging for hypercorn."""

    def __init__(self, config):
        self.access_log_format = config.access_log_format
        self.access_logger = logging.getLogger("hypercorn.access")
        # TODO: decide what to do with the access logs
        self.error_logger = logging.getLogger("hypercorn.error")

    async def access(self, request, response, request_time):
        if self.access_logger is not None:
            # Hack for https://github.com/pgjones/hypercorn/issues/158
            if response is not None:
                self.access_logger.debug(
                    self.access_log_format,
                    self.atoms(request, response, request_time),
                )


class LocalQueueHandler(logging.handlers.QueueHandler):
    # https://www.zopatista.com/python/2019/05/11/asyncio-logging/

    def emit(self, record):
        try:
            self.enqueue(record)
        except asyncio.CancelledError:
            raise
        except Exception:
            self.handleError(record)
