# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
#
# Use of this source code is governed by an MIT-style license that can be
# found in the LICENSE file or at https://opensource.org/licenses/MIT.
from pathlib import Path
import subprocess


def listify(users):
    return "\n".join("- " + u for u in sorted(users, key=str.lower))


def get_all_authors():
    commit_log_authors = subprocess.check_output(
        ["git", "log", "--format=%aN"],
    ).decode("utf-8")
    return {
        x.strip() for x in commit_log_authors.split("\n") if x.strip() != ""
    }


def main():
    """Update CREDITS file with current contributors."""
    maintainers = {
        "Bryan Davis",
        "Kunal Mehta",
        "Merlijn van Deen",
        "YuviPanda",
    }
    ignored = {
        "jenkins-bot",
    }

    contributors = get_all_authors() - maintainers - ignored
    marked_maintainers = {m + " (maintainer)" for m in maintainers}

    with (Path.cwd() / "CREDITS").open("w", encoding="utf-8") as f:
        f.write(
            "We would like to thank all of our contributors for helping improve wikibugs!\n",
        )
        f.write("\n")
        f.write(listify(contributors | marked_maintainers))
        f.write("\n")
