# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
from enum import Enum
import json
import logging
import os
import re

import aiorun
import asyncssh

from . import channelfilter
from . import events
from . import messagebuilder
from . import settings
from . import utils

IGNORED_USERS = ["L10n-bot", "Libraryupgrader"]
IGNORED_POSITIVE_VOTES = ["jenkins-bot", "PipelineBot", "SonarQube Bot"]
JENKINS_USER = "jenkins-bot"

logger = logging.getLogger(__name__)


class IncludeOwner(Enum):
    IF_NOT_USER = 1
    ALWAYS = 2


def extract_bug(commit_msg: str):
    search = re.search(r"Bug: T(\d+)", commit_msg)
    if search:
        return "T" + search.group(1)


def should_ignore_ci_comment(ret, event):
    """Ignore comments from CI bots.

    jenkins-bot and Pipeline bot get a special treatment: their comments are
    only reported in case an approval *is present*, and *changes* to
    a *negative value*. The goal is to filter out post-commit events, code
    coverage comments, etc.

    :param event: The event to handle
    :return: Whether the event should be ignored or not.
    """
    if ret["user"] not in IGNORED_POSITIVE_VOTES:
        return False

    if "approvals" not in event:
        return True

    if not any(
        "oldValue" in approval and int(approval["value"]) < 0
        for approval in event["approvals"]
    ):
        return True

    return False


# T239928
def change_is_wip(event: dict) -> bool:
    return event.get("change", {}).get("wip", False)


def process_event(event: dict):
    user = event.get("uploader", {}).get("name") or event.get(
        "author",
        {},
    ).get("name")
    if user in IGNORED_USERS or change_is_wip(event):
        return None

    ret = None
    if event["type"] == "patchset-created":
        ps = "PS" + str(event["patchSet"]["number"])
        ret = process_simple(event, ps, "uploader", IncludeOwner.IF_NOT_USER)
    elif event["type"] == "wip-state-changed" and not change_is_wip(event):
        ps = "PS" + str(event["patchSet"]["number"])
        ret = process_simple(event, ps, "changer", IncludeOwner.IF_NOT_USER)
    elif event["type"] == "comment-added":
        ret = process_simple(event, "CR", "author")

        if should_ignore_ci_comment(ret, event):
            return None

        comment = ""
        original_comment = event.get("comment")
        inline = 0

        if original_comment:
            inline_match = re.search(r"\((\d+) comments?\)", original_comment)
            if inline_match:
                try:
                    inline = int(inline_match.group(1))
                except ValueError:
                    pass
                # cheat! Get rid of (# comments) from the text
                original_comment = original_comment.replace(
                    inline_match.group(0),
                    "",
                )
            comment = (
                "\n".join(original_comment.split("\n")[1:])
                .strip()
                .split("\n")[0]
                .strip()
            )

        if comment == "This change is ready for review.":
            # T350778: a wip-to-non-wip change results in *both* a wip-state-changed and a comment-added event.
            # Ignore the latter.
            return None

        if comment:
            comment = '"' + comment[:138] + '"'
        else:
            comment = event["change"]["subject"][:140]
        ret["message"] = comment
        if ret["user"] == JENKINS_USER:
            ret["message"] = event["change"]["subject"]
        ret["inline"] = inline
        ret["approvals"] = {}

        if "approvals" in event:
            for approval in event["approvals"]:
                value = int(approval["value"])
                old_value = int(approval.get("oldValue", 0))

                if value == old_value:
                    # if the review value didn't change, don't mention the score
                    continue

                if approval["type"] == "Verified" and value != 0:
                    ret["approvals"]["V"] = value
                    if ret["user"] == JENKINS_USER and value == -1:
                        ret["user"] = "CI reject"
                elif approval["type"] == "Code-Review" and value != 0:
                    ret["approvals"]["C"] = value

    elif event["type"] == "change-merged":
        ret = process_simple(event, "Merged", "submitter")
        if ret["user"] == JENKINS_USER and ret["owner"] in IGNORED_USERS:
            return None
        elif ret["user"] != JENKINS_USER:
            # Ignore any merges by anyone that is not jenkins-bot
            # This is always preceded by a C:2 by them, so we need not spam
            return None
    elif event["type"] == "change-restored":
        ret = process_simple(event, "Restored", "restorer")
    elif event["type"] == "change-abandoned":
        ret = process_simple(event, "Abandoned", "abandoner")

    return ret


def process_simple(
    event: dict,
    type_: str,
    user_property: str,
    include_owner: IncludeOwner = IncludeOwner.ALWAYS,
) -> dict:
    ret = {
        "type": type_,
        "user": event[user_property]["name"],
        "message": event["change"]["subject"],
        "repo": event["change"]["project"],
        "branch": event["change"]["branch"],
        # Use short URL form (T175929#6250620)
        "url": "https://{}/r/{}".format(
            settings.GERRIT_SERVER,
            event["change"]["number"],
        ),
        "task": extract_bug(event["change"]["commitMessage"]),
    }

    owner = event["change"]["owner"]["name"]
    if (include_owner == IncludeOwner.ALWAYS) or (
        include_owner == IncludeOwner.IF_NOT_USER and ret["user"] != owner
    ):
        ret["owner"] = owner

    return ret


async def async_main(verbosity):
    """Process Gerrit event-stream events and enqueue IRC messages."""
    chanfilter = await channelfilter.ChannelFilter.create(
        origin=settings.GERRIT_CHANNELS_FILE,
    )
    builder = messagebuilder.GerritMessageBuilder()
    async with utils.create_aiohttp_client_session() as session:
        writer = events.WriterClient(
            settings.EVENT_SERVER_URL,
            settings.WEB_BEARER_TOKEN,
            session,
        )
        return await process_gerrit_event_stream(
            verbosity,
            writer,
            chanfilter,
            builder,
        )


async def process_gerrit_event_stream(
    verbosity,
    writer,
    chanfilter,
    builder,
):
    # Clamp debug level to [1, 2]. Level 3 can reveal secrets
    asyncssh.set_debug_level(min(max(1, verbosity), 2))
    # T360860#9672029: Asyncssh uses `getpass.getuser()` which can fail when
    # a local username is not easily detected in the environment. For
    # wikibugs2 this can happen when running from a Toolforge Build Service
    # generated container. The value it finds ultimately is unused in our
    # implementation, so we can avoid the potential crash by ensuring that
    # there is some value to be found.
    os.environ["LNAME"] = settings.GERRIT_SSH_USER
    logger.info("Connecting to %s", settings.GERRIT_SERVER)
    async with asyncssh.connect(
        settings.GERRIT_SERVER,
        port=settings.GERRIT_PORT,
        username=settings.GERRIT_SSH_USER,
        client_keys=[
            asyncssh.import_private_key(settings.GERRIT_SSH_KEY),
        ],
        connect_timeout=5,
        login_timeout=5,
        keepalive_interval=10,  # Send a ping every 10 seconds
        keepalive_count_max=3,  # Disconnect after 3 unanswered pings
        known_hosts=None,  # Disable server host key checking
    ) as ssh:
        logger.info("Opened SSH connection for %s", settings.GERRIT_SSH_USER)
        async with ssh.create_process(
            "gerrit stream-events",
            encoding="utf-8",
            stdin=asyncssh.DEVNULL,
            stderr=asyncssh.STDOUT,
        ) as event_stream:
            while True:
                await utils.touch_healthz_file()
                try:
                    async with asyncio.timeout(61):
                        line = await event_stream.stdout.readline()
                except TimeoutError:
                    logger.info("Timeout waiting for event.")
                    await asyncio.sleep(0.1)
                    continue

                line = line.strip()
                logger.info("stream-events: %s", line)
                parsed = json.loads(line)
                processed = process_event(parsed)
                if processed:
                    logger.debug("processed: %s", json.dumps(processed))
                    msg = builder.build_message(processed)
                    updated = await chanfilter.update()
                    if updated:
                        await writer.irc_event(
                            message=f"!log {settings.IRC_SAL_PROJECT} "
                            f"Updated gerrit-channels.yaml to: {updated}",
                            channels=[settings.IRC_SAL_CHANNEL],
                        )
                        logger.info(
                            "Updated %s to: %s",
                            settings.GERRIT_CHANNELS_FILE,
                            updated,
                        )
                    channels = list(
                        chanfilter.channels_for(
                            {
                                processed["repo"]: {
                                    "branch": processed["branch"],
                                },
                            },
                            channelfilter.branch_selector,
                        ).keys(),
                    )
                    await writer.irc_event(message=msg, channels=channels)
                    logger.debug(
                        "message: '%s' to [%s]",
                        msg,
                        ", ".join(channels),
                    )


def main(verbosity):
    """Process Gerrit event-stream events and enqueue IRC messages."""
    return aiorun.run(
        async_main(verbosity),
        stop_on_unhandled_errors=True,
        timeout_task_shutdown=23,
    )
