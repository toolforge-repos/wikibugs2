# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import hashlib
import logging
import os
import time

import aiofiles
import aiohttp
import yaml

from . import settings

logger = logging.getLogger(__name__)


USER_AGENT = "{name} ({url}) python-aiohttp/{vers}".format(
    name="Wikibugs",
    url="https://www.mediawiki.org/wiki/Wikibugs",
    vers=aiohttp.__version__,
)


def create_aiohttp_client_session():
    """Create a new aiohttp.ClientSession instance."""
    return aiohttp.ClientSession(
        connector=aiohttp.TCPConnector(keepalive_timeout=37),
        headers={"User-Agent": USER_AGENT},
        trust_env=True,
        timeout=aiohttp.ClientTimeout(connect=3, sock_connect=3),
    )


async def fetch_channel_config(url):
    """Fetch channel config data from a URL."""
    logger.info("fetch_channel_config(%s)", url)
    if url.startswith("file://"):
        return fetch_channel_file(url[7:])
    async with create_aiohttp_client_session() as session:
        async with session.get(url) as resp:
            resp.raise_for_status()
            src = await resp.text()
            return parse_channel_config(src)


def fetch_channel_file(path):
    """Fetch channel config data from a file."""
    logger.info("fetch_channel_file(%s)", path)
    with open(path, encoding="utf-8") as f:
        return parse_channel_config(f.read())


def parse_channel_config(src):
    """Parse channel config from a string."""
    config = yaml.safe_load(src)
    config["sha256sum"] = hashlib.sha256(
        src.encode("utf-8"),
        usedforsecurity=False,
    ).hexdigest()
    if "default-channel" in config:
        # XXX: Temporary fix up needed for legacy config.
        logger.debug("Fixing up legacy channel config.")
        config["default_channel"] = config.pop("default-channel")
        config["firehose_channel"] = config.pop("firehose-channel")
    return config


async def touch_healthz_file():
    """Write the current timestamp to settings.HEALTH_CHECK_FILE."""
    tmp_file = f"{settings.HEALTH_CHECK_FILE}--next"
    async with aiofiles.open(tmp_file, mode="w+") as f:
        await f.write(str(int(time.monotonic())))
    os.rename(tmp_file, settings.HEALTH_CHECK_FILE)


def check_healthz(max_lag):
    """Read settings.HEALTH_CHECK_FILE and compare to current timestamp."""
    now = int(time.monotonic())
    try:
        with open(settings.HEALTH_CHECK_FILE) as f:
            last = int(f.read())
        lag = now - last
        logger.debug("Healthz lag: %s", lag)
        return lag < max_lag
    except (OSError, ValueError):
        logger.exception("Failed to read %s", settings.HEALTH_CHECK_FILE)
        return False
