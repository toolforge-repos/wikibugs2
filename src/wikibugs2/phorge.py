# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
import json
import logging

import aiohttp
import aiorun

from . import events
from . import settings
from . import utils

logger = logging.getLogger(__name__)


class ConduitError(Exception):
    def __init__(self, message, code, result):
        self.message = message
        self.code = code
        self.result = result

    def __str__(self):
        return f"{self.message} ({self.code})"


class ConduitClient:
    """Phorge Conduit API client."""

    def __init__(self, url, token, session):
        self.url = url
        self.conduit_token = {"token": token}
        self.session = session

    async def post(self, path, data):
        data["__conduit__"] = self.conduit_token
        async with self.session.post(
            f"{self.url}/api/{path}",
            data={
                "params": json.dumps(data),
                "output": "json",
            },
            timeout=13,
        ) as r:
            resp = await r.json()
            logger.debug("%s result: %s", path, resp)
            if resp["error_code"] is not None:
                raise ConduitError(
                    resp["error_info"],
                    resp["error_code"],
                    resp.get("result", None),
                )
            return resp["result"]


class PhorgeFeedReader:

    def __init__(
        self,
        session=None,
        writer=None,
        ask_before_push=False,
        start_from=0,
    ):
        self.ask_before_push = ask_before_push
        self.client = ConduitClient(
            url=settings.PHAB_HOST,
            token=settings.PHAB_TOKEN,
            session=session,
        )
        self.writer = writer
        self.poll_last_seen_chrono_key = start_from
        self.empty_polls = 0

    async def get_user_name(self, phid):
        """
        :param phid: A PHID- thingy representing a user
        :type phid: string
        """
        info = await self.client.post(
            "user.query",
            {
                "phids": [phid],
            },
        )
        return info[0]["userName"]

    async def poll(self):
        await utils.touch_healthz_file()
        if self.poll_last_seen_chrono_key == 0:
            # First time, get the latest event and start from there
            result = await self.client.post(
                "feed.query",
                {
                    "limit": "1",
                },
            )
            latest = list(result.values())[0]
            self.poll_last_seen_chrono_key = int(latest["chronologicalKey"])
            logger.info(
                "Initial chronologicalKey: %s",
                self.poll_last_seen_chrono_key,
            )

        try:
            events = await self.client.post(
                "feed.query",
                {
                    "view": "data",
                    "before": self.poll_last_seen_chrono_key,
                },
            )
        except (TimeoutError, aiohttp.ClientError, ConduitError):
            logger.exception("Failed to poll feed.query for new events")
            # Make events an empty list matching the JSON response when there
            # are no new events to process. This will trigger backoff behavior
            # below.
            events = []

        if not events:
            # Nothing in the queue for us to process.
            # Note: due to PHP quirks, events will be an empty list
            self.empty_polls += 1
            backoff = min(0.008 * 2**self.empty_polls, 7)
            logger.debug(
                "%d empty polls. Sleeping %.3fs",
                self.empty_polls,
                backoff,
            )
            await asyncio.sleep(backoff)
            return
        else:
            self.empty_polls = 0

        for event in sorted(
            events.values(),
            key=lambda x: int(x["chronologicalKey"]),
        ):
            key = int(event["chronologicalKey"])
            if key > self.poll_last_seen_chrono_key:
                await self.process_event(event)
                self.poll_last_seen_chrono_key = key

    async def phid_info(self, phids):
        """Get information on a list of PHIDs.

        :returns: dict keyed by PHID
        """
        if not phids:
            return {}
        info = await self.client.post(
            "phid.query",
            {
                "phids": phids,
            },
        )
        logger.debug("phid_info(%r): %s", phids, info)
        return info

    async def maniphest_info(self, task_id):
        """
        :param task_id: T###
        :type task_id: basestring
        """
        task_id = int(task_id[1:])
        info = await self.client.post(
            "maniphest.info",
            {
                "task_id": task_id,
            },
        )
        logger.debug("maniphest.info for %r = %s", task_id, json.dumps(info))
        return info

    async def project_info(self, phids):
        """Get project information for a list of PHIDs."""
        if not phids:
            return {}
        info = await self.client.post(
            "project.search",
            {
                "constraints": {
                    "phids": phids,
                },
            },
        )
        data = info["data"]
        logger.debug("project.search for %r = %s", phids, data)
        return data

    async def get_transaction_info(self, task_id, transaction_phids):
        """
        :param task_id: T###
        :type task_id: basestring
        :param transaction_phids: PHID-XACT-TASK-* stuff
        :type transaction_phids: list
        """
        task_id = int(task_id[1:])
        info = await self.client.post(
            "maniphest.gettasktransactions",
            {
                "ids": [task_id],
            },
        )
        transactions = {}
        for trans in list(info.values())[0]:
            if trans["transactionPHID"] in transaction_phids:
                ttype = trans["transactionType"]
                if ttype == "core:customfield":
                    # Treat custom field key as transaction type
                    ttype = trans["meta"]["customfield:key"]
                transactions[ttype] = {
                    "old": trans["oldValue"],
                    "new": trans["newValue"],
                    "anchor": trans["transactionID"],
                }
                if trans["comments"] is not None:
                    transactions[ttype]["comments"] = trans["comments"]
                if "core.create" in trans["meta"]:
                    # This was part of the initial object creation.
                    # Force anchor to "0" so bare link will be used later
                    transactions[ttype]["anchor"] = "0"
        logger.debug(
            "get_transaction_info(%r,%r) = %s",
            task_id,
            transaction_phids,
            json.dumps(transactions),
        )
        return transactions

    async def get_tags(self, task_info):
        """
        :param task_info: Data from the maniphest.info API
        :type task_info: dict
        """
        alltags = {}
        logger.debug("get_tags(%s)", json.dumps(task_info))

        project_info = await self.project_info(task_info["projectPHIDs"])
        phid_info_dict = await self.phid_info(task_info["projectPHIDs"])
        for info in project_info:
            phid = info["phid"]
            tag = info["fields"]
            phid_info = phid_info_dict[phid]

            name = tag["name"]
            if tag["milestone"] and tag["parent"]:
                # Construct fully qualified milestone name
                name = f"{tag['parent']['name']} ({name})"
            alltags[name] = {
                "shade": tag["color"]["key"],
                "disabled": phid_info["status"] == "closed",
                "tagtype": tag["icon"]["icon"][3:],
                "uri": phid_info["uri"],
            }
        return alltags

    def guess_best_anchor(self, transactions):
        """Pick the best anchor we can find.

        :param transactions: simplified transaction info
        :type transactions: list(dict)
        :returns: basestring
        """
        logger.debug("guess_best_anchor(%s)", transactions)
        # Our current theory is that the lowest (oldest) transaction id in the
        # set is what Phorge will be using as an anchor id in the rendered
        # HTML. We know however that this is only true after discarding some
        # transaction types, so it is a fragile heuristic at best.
        anchors = [trans["anchor"] for trans in transactions.values()]
        min_anchor = sorted(anchors, key=lambda x: int(x))[0]
        if min_anchor != "0":
            return f"#{min_anchor}"
        return ""

    def get_type_from_phid(self, phid):
        """Extract an object type from a PHID string.

        :param phid: PHID-TASK-``*`` or PHID-CMIT-``*``, etc
        :return: str
        """
        return phid.split("-")[1]

    async def process_event(self, event_info):
        """
        :type event_info: dict
        """
        phid_type = self.get_type_from_phid(event_info["data"]["objectPHID"])
        if phid_type != "TASK":  # Only handle Maniphest Tasks for now
            logger.debug(
                "Skipping %s, it is of type %s",
                event_info["data"]["objectPHID"],
                phid_type,
            )
            return

        if "transactionPHIDs" not in event_info["data"]:
            logger.debug(
                "Skipping %s, does not contain transactions",
                event_info["data"]["objectPHID"],
            )
            # If there are no transactions, it's something weird like tokens
            return

        logger.info(
            "Processing event (start_from: %s) %s",
            self.poll_last_seen_chrono_key,
            json.dumps(event_info),
        )

        phid = event_info["data"]["objectPHID"]
        phid_info_dict = await self.phid_info([phid])
        phid_info = phid_info_dict[phid]
        task_info = await self.maniphest_info(phid_info["name"])

        transactions = await self.get_transaction_info(
            phid_info["name"],
            event_info["data"]["transactionPHIDs"],
        )
        ignored = [
            "core:columns",  # Column changes, see T1204
            "core:edit-policy",  # Edit policy changed
            "core:file",  # Attached/removed/modified file
            "core:inlinestate",  # Changed inline comment state
            "core:interact-policy"  # Interaction policy changed
            "core:join-policy",  # Join policy changed
            "core:space",  # Moved to a different space
            "core:subscribers",  # CC updates
            "core:subtype",  # Subtype changes
            "core:view-policy",  # View policy changed
            "std:maniphest:security_topic",  # T105625
            "token:give",  # Adding a token
            "unblock",  # T357875: add, remove, resolve subtask
        ]
        removed_types = []
        for event_type in ignored:
            if event_type in transactions:
                removed_types.append(event_type)
                transactions.pop(event_type)

        if not transactions:
            # We removed everything, skip.
            logging.debug(
                "Skipping %s which only has event types: %s",
                event_info["data"]["transactionPHIDs"],
                ", ".join(removed_types),
            )
            return

        anchor = self.guess_best_anchor(transactions)
        projects = await self.get_tags(task_info)

        # Start sorting this into things we care about...
        useful_event_metadata = {
            "url": phid_info["uri"] + anchor,
            "projects": projects,
            "user": await self.get_user_name(event_info["authorPHID"]),
            "is_closed": task_info["isClosed"],
        }

        if "core:create" in transactions:
            useful_event_metadata["new"] = True

        if "title" in transactions:
            useful_event_metadata["title"] = transactions["title"]["new"]
        else:
            # Technically there's a race condition if the title is changed
            # in another event before our API request is made, but meh
            # Name is in the format "T123: FooBar", so get rid of the prefix
            useful_event_metadata["title"] = (
                phid_info["fullName"].split(":", 1)[1].strip()
            )

        if (
            "core:comment" in transactions
            and "comments" in transactions["core:comment"]
        ):
            useful_event_metadata["comment"] = transactions["core:comment"][
                "comments"
            ]

        for _type in ["status", "priority"]:
            if _type in transactions:
                useful_event_metadata[_type] = transactions[_type]

        if (
            "status" in useful_event_metadata
            and useful_event_metadata["status"]["old"] is None
        ):
            useful_event_metadata["new"] = True
            useful_event_metadata["url"] = useful_event_metadata["url"].split(
                "#",
            )[0]

        if "reassign" in transactions:
            trans = transactions["reassign"]
            info = {}
            for _type in ["old", "new"]:
                if trans[_type] is not None:
                    info[_type] = await self.get_user_name(trans[_type])
                else:
                    info[_type] = None
            useful_event_metadata["assignee"] = info

        if "mergedinto" in transactions:
            # T128868: Show when a task has been closed as a duplicate
            merged_into = transactions["mergedinto"]
            phid_info_dict = await self.phid_info([merged_into["new"]])
            info = phid_info_dict[merged_into["new"]]
            merged_into["new"] = info["name"]
            useful_event_metadata["mergedinto"] = merged_into

        logger.debug(useful_event_metadata)
        if (
            not self.ask_before_push
            or input("Push? (y/N)").lower().strip() == "y"
        ):
            await self.writer.phorge_event(useful_event_metadata)

    def dump_error(self, error_type, e, event_info):
        # FIXME: Dump path should be configurable
        # FIXME: make async
        open(
            "/data/project/wikibugs/errors/"
            + error_type
            + "/"
            + event_info["data"]["objectPHID"],
            "w",
        ).write(
            repr(event_info) + "\n" + repr(e),
        )


async def async_main(ask_before_push, start_from, files):
    async with utils.create_aiohttp_client_session() as session:
        writer = events.WriterClient(
            settings.EVENT_SERVER_URL,
            settings.WEB_BEARER_TOKEN,
            session,
        )
        bugs = PhorgeFeedReader(
            session=session,
            writer=writer,
            ask_before_push=ask_before_push,
            start_from=start_from,
        )

        if files:
            for file in files:
                logger.info("Processing %s", file)
                from collections import OrderedDict  # noqa

                await bugs.process_event(eval(open(file).readline()))
            return

        logger.info("Starting polling cycle")
        while True:
            await bugs.poll()
            await asyncio.sleep(1)


def main(ask_before_push=False, start_from=0, files=None):
    """Process Phorge events and enqueue data for creating IRC messages."""
    return aiorun.run(
        async_main(ask_before_push, start_from, files),
        stop_on_unhandled_errors=True,
        timeout_task_shutdown=23,
    )
