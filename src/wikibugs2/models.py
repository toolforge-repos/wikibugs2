# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import json
import logging

import msgspec

logger = logging.getLogger(__name__)


class Struct(msgspec.Struct):
    """Base class for our local models."""

    def to_dict(self):
        return {f: getattr(self, f) for f in self.__struct_fields__}

    @classmethod
    def from_json(cls, json):
        return msgspec.json.decode(json, type=cls)


class ChannelFilterConfig(Struct):
    """Channelfilter configuration data."""

    default_channel: str
    firehose_channel: str
    channels: dict
    sha256sum: str


class IrcMessage(Struct, kw_only=True):
    """Message to be sent to irc."""

    message: str
    channels: list[str]


class PhorgeEventMetadata(Struct):
    """Metadata about an event that happened in Phorge."""

    title: str
    url: str
    user: str
    projects: dict
    is_closed: bool = False
    new: bool = False
    comment: str | None = None
    status: dict | None = None
    priority: dict | None = None
    assignee: dict | None = None
    mergedinto: dict | None = None


class EmptyBody(Struct):
    """Empty body"""

    pass


class MissingToken(Struct):
    """Missing token"""

    error: str


class InvalidToken(Struct):
    """Invalid token"""

    error: str


class ApiError(Struct):
    """API error"""

    errors: list[str] = []


class ServerSentEvent(Struct, kw_only=True):
    event: str | None = None
    id: int | None = None  # noqa: A003 shadowing a Python builtin
    retry: int | None = None
    comment: str | None = None
    data: str | dict | list | Struct | None = None

    def encode(self):
        """Encode as a bytearray."""
        msg = []
        if self.comment is not None:
            msg.append(f": {self.comment}")
        if self.event is not None:
            msg.append(f"event: {self.event}")
        if self.id is not None:
            msg.append(f"id: {self.id}")
        if self.retry is not None:
            msg.append(f"retry: {self.retry}")
        if self.data is not None:
            if isinstance(self.data, Struct):
                self.data = self.data.to_dict()
            if not isinstance(self.data, str):
                self.data = json.dumps(self.data)
            for line in self.data.splitlines():
                msg.append(f"data: {line}")
        if msg:
            msg_str = "\n".join(msg)
            return f"{msg_str}\n\n".encode()
        else:
            return b":empty packet\n\n"
