# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
import json
import logging

import aiohttp

from . import models

logger = logging.getLogger(__name__)


class WriterClient:
    """Wikibugs2 event API client."""

    def __init__(self, server_url, token, session):
        self.server_url = server_url
        self.token = f"Bearer {token}"
        self.session = session

    async def put(self, path, data):
        """Submit an event for processing."""
        logger.debug("put(%s, %r)", path, data)
        if isinstance(data, models.Struct):
            data = data.to_dict()

        async with self.session.put(
            f"{self.server_url}/api/event/{path}",
            json=data,
            headers={"Authorization": self.token},
            timeout=13,
        ) as r:
            resp = await r.json()
            logger.debug("%s result: %s", path, resp)
            # FIXME: what should really happen here?
            r.raise_for_status()
            return True

    async def irc_event(self, message, channels):
        """Submit an IRC message event."""
        return await self.put(
            path="irc",
            data=models.IrcMessage(
                message=message,
                channels=channels,
            ).to_dict(),
        )

    async def phorge_event(self, metadata):
        """Submit an Phorge event."""
        return await self.put(
            path="phorge",
            data=models.PhorgeEventMetadata(**metadata).to_dict(),
        )


class SSEClient:
    """Wikibugs2 SSE client."""

    # NOTE: This is not a general purpose SSE client. It is specifically
    # tuned for use with the Wikibugs2 SSE server.

    def __init__(self, feed_url, token, session):
        self.feed_url = feed_url
        if token:
            self.token = f"Bearer {token}"
        else:
            self.token = None
        self.session = session
        self.disconnects = 0
        self._sse_data_feed = None

    async def __aenter__(self):
        """Connect and listen for events."""
        self.disconnects = 0
        await self._connect()
        logger.debug("Connected to %s", self.feed_url)
        return self

    async def __aexit__(self, *exc):  # noqa: U100 Unused argument
        """Disconnect from SSE server."""
        await self.close()

    def __aiter__(self):
        """Get an async iterator."""
        return self

    async def __anext__(self):
        """Fetch next async iterator value."""
        if not self._sse_data_feed:
            raise ValueError("SSE data feed has not been established.")
        while self._sse_data_feed.status != 204:
            event = models.ServerSentEvent()
            async for bline in self._sse_data_feed.content:
                line = bline.decode("utf8")
                line = line.rstrip("\r\n")
                logger.debug("line: %r", line)

                if line == "":
                    # end of record
                    if event.data and isinstance(event.data, str):
                        # We emit data as json blobs.
                        event.data = json.loads(event.data)
                    return event

                if line[0] == ":":
                    event.comment = line[1:]
                    continue

                if ":" in line:
                    key, value = (s.lstrip() for s in line.split(":", 1))
                    if key == "data":
                        if not event.data:
                            event.data = ""
                        if event.data:
                            event.data += "\n"
                        event.data += value
                    elif key in {"event", "id", "comment"}:
                        setattr(event, key, value)
                    else:
                        logger.warning("Unexpected line: %s", line)
                else:
                    logger.warning("Unexpected line: %s", line)

            # input stream ended
            logger.info("Stream ended")
            await self._reconnect()
        raise StopAsyncIteration

    async def _connect(self):
        """Connect to SSE server's data stream."""
        logger.info("Connecting to %s", self.feed_url)
        headers = {
            "Accept": "text/event-stream",
            "Cache-Control": "no-cache",
            "Connection": "keep-alive",
        }
        if self.token:
            headers["Authorization"] = self.token
        try:
            resp = await self.session.get(self.feed_url, headers=headers)
            logger.debug(resp)
            resp.raise_for_status()
            if resp.content_type != "text/event-stream":
                raise ConnectionAbortedError(
                    f"{self.feed_url} returned wrong content-type: ",
                    resp.content_type,
                )
            self._sse_data_feed = resp
        except (aiohttp.ClientError, ConnectionAbortedError):
            logger.exception("Failed to connect to %s", self.feed_url)
            await self._reconnect()

    async def close(self):
        """Disconnect from SSE server."""
        if self._sse_data_feed:
            self._sse_data_feed.close()
            self._sse_data_feed = None

    async def _reconnect(self):
        """Reconnect to SSE server after an unexpected interruption."""
        self.disconnects += 1
        await self.close()
        backoff = min(0.008 * 2**self.disconnects, 7)
        logger.debug(
            "%d disconnects. Sleeping %.3fs",
            self.disconnects,
            backoff,
        )
        await asyncio.sleep(backoff)
        await self._connect()
