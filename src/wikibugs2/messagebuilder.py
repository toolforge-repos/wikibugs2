# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import enum


class Color(str, enum.Enum):
    """IRC color codes.

    The following colors are safe for use on both black and white backgrounds:
    green, red, brown, purple, orange, teal, cyan, royal, pink, grey, silver

    Make sure to define a background when using other colors!
    """

    WHITE = "00"
    BLACK = "01"
    BLUE = "02"
    GREEN = "03"
    RED = "04"
    BROWN = "05"
    PURPLE = "06"
    ORANGE = "07"
    YELLOW = "08"
    LIME = "09"
    TEAL = "10"
    CYAN = "11"
    ROYAL = "12"
    PINK = "13"
    GREY = "14"
    SILVER = "15"

    def __str__(self):
        return self.value


class Style(str, enum.Enum):
    BOLD = "\x02"
    ITALIC = "\x1d"
    UNDERLINE = "\x1f"
    STRIKE = "\x1e"
    MONOSPACE = "\x11"
    REVERSE = "\x16"
    COLOR = "\x03"
    RESET = "\x0f"

    def __str__(self):
        return self.value


class IRCMessageBuilder:
    """Fluent builder for creating IRC message strings."""

    MAX_MESSAGE_LENGTH = 80 * 4

    def __init__(self):
        self.buffer = []

    def fmt(self, text, fg=None, bg=None, style=None, as_array=False):
        """Format a string."""
        buf = []
        if any([fg, bg]):
            buf.append(Style.COLOR)
        if fg:
            buf.append(fg)
        if bg:
            buf.extend([",", bg])
        if style:
            buf.append(style)
        buf.append(text)
        if any([fg, bg, style]):
            buf.append(Style.RESET)
        if as_array:
            return buf
        return "".join([str(item) for item in buf])

    def fmta(self, *args, **kwargs):
        """Build a format array."""
        return self.fmt(*args, **kwargs, as_array=True)

    def esc(self, text):
        """Escape user supplied text so it can't be abused to execute
        arbitrary IRC commands.

        :param text: possibly unsafe input
        :return: safe output
        """
        return (
            text.replace("\n", " ")
            .replace("\r", " ")
            .replace("```", "`")
            .replace("\t", " ")
            .replace("#page", "# page")  # T281105
        )

    def extend(self, args):
        self.buffer.extend([str(arg) for arg in args])
        return self

    def text(self, *args):
        return self.extend(args)

    def __str__(self):
        return "".join(self.buffer)


class GerritMessageBuilder:
    """Build IRC messages for Gerrit events."""

    def repo(self, repo: str) -> str:
        if repo.startswith("mediawiki/") or repo.startswith("operations/"):
            repo = repo.split("/", 1)[-1]
        return repo

    def approval(self, value: int) -> list:
        msg = IRCMessageBuilder()
        if value == 1:
            return msg.fmta("+1", fg=Color.GREEN)
        elif value == 2:
            return msg.fmta("+2", fg=Color.GREEN, style=Style.BOLD)
        elif value == -1:
            return msg.fmta("-1", fg=Color.RED)
        else:  # -2
            return msg.fmta(value, fg=Color.RED, style=Style.BOLD)

    def build_message(self, event: dict) -> str:
        msg = IRCMessageBuilder()
        msg.text("(", *msg.fmta(event["type"], fg=Color.GREEN), ")")
        msg.text(
            " ",
            *msg.fmta(event["user"], fg=Color.TEAL, style=Style.BOLD),
            ":",
        )
        if "approvals" in event and event["approvals"]:
            has_c = "C" in event["approvals"]
            has_v = "V" in event["approvals"]
            msg.text(" [")
            if has_v:
                msg.text("V:", *self.approval(event["approvals"]["V"]))
            if has_v and has_c:
                msg.text(" ")
            if has_c:
                msg.text("C:", *self.approval(event["approvals"]["C"]))
            msg.text("]")
        msg.text(" ", msg.esc(event["message"]))
        if "inline" in event and event["inline"]:
            msg.text(
                " (",
                *msg.fmta(event["inline"], fg=Color.GREEN, style=Style.BOLD),
                " ",
                "comments" if event["inline"] > 1 else "comment",
                ")",
            )
        msg.text(" [", self.repo(event["repo"]), "]")
        if event["branch"] not in ("master", "main", "production"):
            msg.text(" (", event["branch"], ")")
        msg.text(" - ", *msg.fmta(event["url"], fg=Color.TEAL))
        if event["task"]:
            msg.text(
                " (https://phabricator.wikimedia.org/",
                event["task"],
                ")",
            )
        if "owner" in event:
            msg.text(
                " (owner: ",
                *msg.fmta(event["owner"], fg=Color.TEAL, style=Style.BOLD),
                ")",
            )

        return str(msg)


class PhorgeMessageBuilder:
    """Build IRC messages for Phorge events."""

    MAX_NUM_PROJECTS = 4

    PHAB_COLORS = {
        # Matches phabricator project colors to IRC colors
        "blue": Color.TEAL,
        "red": Color.BROWN,
        "orange": Color.BROWN,
        "yellow": Color.ORANGE,
        "indigo": Color.ROYAL,
        "violet": Color.PURPLE,
        "green": Color.GREEN,
        "grey": Color.GREY,
        "pink": Color.PINK,
        "checkered": Color.SILVER,
        "disabled": Color.GREY,
    }

    PRIORITY = {
        "100": "Unbreak!",
        "90": "Triage",
        "80": "High",
        "50": "Medium",
        "25": "Low",
        "10": "Lowest",
    }

    STATUSES = {
        "open": "Open",
        "needsinfo": "Needs info",
        "invalid": "Invalid",
        "resolved": "Resolved",
        "declined": "Declined",
        "duplicate": "Duplicate",
        "stalled": "Stalled",
        "progress": "In progress",
    }

    OUTPUT_PROJECT_TYPES = ("briefcase", "users", "umbrella")
    # T180293: Hide User-XX projects from wikibugs output
    HIDDEN_PROJECT_TYPES = ("user",)

    def status(self, name):
        return self.STATUSES.get(name, name)

    def prio(self, name):
        return self.PRIORITY.get(str(name), str(name))

    def project_text(self, all_projects, matched_projects):
        """Build project text to be shown in message.

        Requirements:
            (1) Show matched projects first, and bold
            (2) Next, show other projects if they are in OUTPUT_PROJECT_TYPES
            (3) Then other tags
            (4) But never show disabled text or HIDDEN_PROJECT_TYPES
            (5) Unless there aren't any tags at all otherwise
            (6) If there are no projects at all, show "(no projects)" in bright red
            (7) Never show more than MAX_NUM_PROJECTS, even if they are matched

        :param all_projects: dict[project name, info] (scraped) or
                             list[project name] (failed scraping)
        :param matched_projects: list[project name]
        :return: list with formatted projects
        """
        bldr = IRCMessageBuilder()

        # (3) format all projects
        # and map to a standardized format in the process
        projects = {}
        for project in all_projects:
            try:
                info = all_projects[project]
            except KeyError:
                info = {
                    "shade": "green",
                    "tagtype": "briefcase",
                    "disabled": False,
                    "uri": "",
                }
            info["matched"] = project in matched_projects

            color = self.PHAB_COLORS.get(info["shade"], Color.TEAL)
            info["irc_text"] = bldr.fmt(project, fg=color)

            projects[project] = info

        # We map projects in four categories:
        matched_parts = []
        other_projects = []
        other_tags = []
        hidden_parts = []

        # We use them in that order, limiting to N (or (N-1) + 'and M others')
        # projects
        for project in sorted(projects.keys(), key=str.lower):
            info = projects[project]
            if info["matched"]:
                matched_parts.append(info["irc_text"])
            elif info["disabled"]:
                hidden_parts.append(info["irc_text"])
            elif info["tagtype"] in self.HIDDEN_PROJECT_TYPES:
                hidden_parts.append(info["irc_text"])
            elif info["tagtype"] in self.OUTPUT_PROJECT_TYPES:
                other_projects.append(info["irc_text"])
            else:
                other_tags.append(info["irc_text"])

        # (1), (2), (3), (4)
        show_parts = matched_parts + other_projects + other_tags

        # (5), (6)
        if len(show_parts) == 0:
            show_parts = hidden_parts
        if len(show_parts) == 0:
            show_parts = [
                bldr.fmt("(no projects)", fg=Color.RED, style=Style.BOLD),
            ]

        # (7)
        overflow_parts = show_parts[self.MAX_NUM_PROJECTS :]  # noqa: E203
        show_parts = show_parts[: self.MAX_NUM_PROJECTS]

        if len(overflow_parts) == 1:
            show_parts.append(overflow_parts[0])
        elif len(overflow_parts) > 0:
            show_parts.append("and %i others" % len(overflow_parts))
        return ", ".join(show_parts)

    def build_message(self, event):
        status_style = Style.STRIKE if event.get("is_closed", False) else None
        msg = IRCMessageBuilder()
        msg.text(
            self.project_text(event["projects"], event["matched_projects"]),
            ": ",
            *msg.fmta(msg.esc(event["title"]), style=status_style),
            " - ",
            *msg.fmta(event["url"], style=status_style),
            " (",
            *msg.fmta(event["user"], fg=Color.TEAL),
            ") ",
        )
        if event.get("new"):
            msg.text(*msg.fmta("NEW", fg=Color.GREEN), " ")
        elif event.get("status"):
            status = event["status"]
            msg.text(
                *msg.fmta(self.status(status["old"]), fg=Color.BROWN),
                "→",
                *msg.fmta(self.status(status["new"]), fg=Color.GREEN),
                " ",
            )
        elif event.get("mergedinto"):
            msg.text(
                "→",
                *msg.fmta(self.status("duplicate"), fg=Color.GREY),
                " dup:",
                *msg.fmta(event["mergedinto"]["new"], fg=Color.GREEN),
                " ",
            )

        if event.get("priority"):
            prio = event["priority"]
            msg.text("p:")
            if prio["old"]:
                msg.text(
                    *msg.fmta(self.prio(prio["old"]), fg=Color.BROWN),
                    "→",
                    *msg.fmta(self.prio(prio["new"]), fg=Color.GREEN),
                    " ",
                )

        if event.get("assignee"):
            ass = event["assignee"]
            msg.text("a:")
            if ass["old"]:
                msg.text(*msg.fmta(ass["old"], fg=Color.BROWN), "→")
            msg.text(*msg.fmta(str(ass["new"]), fg=Color.GREEN), " ")

        if event.get("comment"):
            msg.text(msg.esc(event["comment"]))

        text = str(msg)
        if len(text) > IRCMessageBuilder.MAX_MESSAGE_LENGTH:
            text = (
                text[: IRCMessageBuilder.MAX_MESSAGE_LENGTH - 3].rstrip()
                + "..."
            )

        # Make sure the URL is always fully present
        if event["url"] not in text:
            inserttext = "... - " + event["url"]
            text = text[: -len(inserttext)] + inserttext

        return text


class GitLabMessageBuilder:
    """Build IRC messages for GitLab events."""

    def build_message(self, event):
        msg = IRCMessageBuilder()
        msg.text("(", *msg.fmta(event["action"], fg=Color.GREEN), ")")
        msg.text(
            " ",
            *msg.fmta(event["user"], fg=Color.TEAL, style=Style.BOLD),
            ":",
        )
        msg.text(" ", msg.esc(event["message"]))
        msg.text(" [", event["repo"], "]")
        if event["branch"] not in ("master", "main", "production"):
            msg.text(" (", event["branch"], ")")
        msg.text(" - ", *msg.fmta(event["url"], fg=Color.TEAL))
        if "tasks" in event and event["tasks"]:
            msg.text(
                " (",
                " ".join(
                    [
                        f"https://phabricator.wikimedia.org/{task}"
                        for task in sorted(
                            event["tasks"],
                            key=lambda x: x[1:],
                        )
                    ],
                ),
                ")",
            )
        if "owner" in event and event["owner"] != event["user"]:
            msg.text(
                " (owner: ",
                *msg.fmta(event["owner"], fg=Color.TEAL, style=Style.BOLD),
                ")",
            )

        return str(msg)
