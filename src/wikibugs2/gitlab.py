# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
import json
import logging
import re

import aiohttp
import aiorun

from . import channelfilter
from . import events
from . import messagebuilder
from . import settings
from . import utils

logger = logging.getLogger(__name__)
RE_TASK_IDS = re.compile(r"^Bug:\s*(T\d+)\s*$", flags=re.MULTILINE)


class APIError(Exception):
    def __init__(self, message, code, result):
        self.message = message
        self.code = code
        self.result = result

    def __str__(self):
        return f"{self.message} ({self.code})"


class Client:
    """GitLab API client."""

    def __init__(self, url, token, session):
        self.url = url
        self.token = token
        self.session = session

    async def request(self, verb, path, **kwargs):
        if "headers" not in kwargs:
            kwargs["headers"] = {}
        kwargs["headers"]["PRIVATE-TOKEN"] = self.token
        kwargs["headers"]["Content-Type"] = "application/json"
        if "timeout" not in kwargs:
            kwargs["timeout"] = 13
        url = f"{self.url}/api/v4{path}"
        async with self.session.request(method=verb, url=url, **kwargs) as r:
            if r.ok:
                return await r.json()
            err_msg = r.content
            try:
                err_json = await r.json()
                if "message" in err_json:
                    err_msg = err_json["message"]
                elif "error" in err_json:
                    err_msg = err_json["error"]
            except json.decoder.JSONDecodeError:
                logger.exception(
                    "Failed to parse error message from %s: %s",
                    url,
                    err_msg,
                )
            raise APIError(err_msg, r.status, r)

    async def get(self, path, params=None):
        resp = await self.request("GET", path, params=params)
        logger.debug("GET %s: %s", path, resp)
        return resp

    async def user_info(self, user_id):
        """Get information for a given user id."""
        return await self.get(f"/users/{user_id}")


async def async_main():
    """Process GitLab webhook events and enqueue IRC messages."""
    chanfilter = await channelfilter.ChannelFilter.create(
        origin=settings.GITLAB_CHANNELS_FILE,
    )
    builder = messagebuilder.GitLabMessageBuilder()
    async with utils.create_aiohttp_client_session() as session:
        writer = events.WriterClient(
            settings.EVENT_SERVER_URL,
            settings.WEB_BEARER_TOKEN,
            session,
        )
        client = Client(settings.GITLAB_HOST, settings.GITLAB_TOKEN, session)
        async with events.SSEClient(
            settings.GITLAB_EVENTS_URL,
            None,
            session,
        ) as reader:
            try:
                async for event in reader:
                    await handle_event(
                        event,
                        chanfilter,
                        builder,
                        writer,
                        client,
                    )
                    await asyncio.sleep(0.1)
            except aiohttp.ClientError:
                logger.exception("Client failure reading from SSE service")
                return


async def handle_event(event, chanfilter, builder, writer, gitlab_client):
    """Handle a server sent event."""
    await utils.touch_healthz_file()
    if event.event == "webhook":
        logger.info("webhook: %s", event.data)
        processed = await process_webhook(event.data, gitlab_client)
        if processed:
            logger.debug("processed: %s", processed)
            msg = builder.build_message(processed)

            updated = await chanfilter.update()
            if updated:
                await writer.irc_event(
                    message=f"!log {settings.IRC_SAL_PROJECT} "
                    f"Updated gitlab-channels.yaml to: {updated}",
                    channels=[settings.IRC_SAL_CHANNEL],
                )
                logger.info(
                    "Updated %s to: %s",
                    settings.GITLAB_CHANNELS_FILE,
                    updated,
                )
            channels = list(
                chanfilter.channels_for(
                    {
                        processed["repo"]: {
                            "branch": processed["branch"],
                        },
                    },
                    channelfilter.branch_selector,
                ).keys(),
            )
            await writer.irc_event(message=msg, channels=channels)
            logger.debug(
                "message: '%s' to [%s]",
                msg,
                ", ".join(channels),
            )

    elif event.event == "keepalive":
        logger.info("Received keepalive from server.")

    else:
        logger.warning("Unexpected event: %r", event)


async def process_webhook(event, gitlab_client):
    """Process a webhook into a dict of message data or None to ignore."""
    kind = event.get("object_kind")

    if kind == "merge_request":
        return await process_mr(event, gitlab_client)
    return None


async def process_mr(event, gitlab_client):
    """Process a merge_request event into a dict of message data or None."""
    attribs = event.get("object_attributes")
    if attribs is None:
        return None

    action = attribs.get("action")
    if action is None:
        return None

    data = {
        "action": action,
        "user": event["user"]["username"],
        "message": attribs["title"],
        "repo": attribs["target"]["path_with_namespace"],
        "branch": attribs["target_branch"],
        "url": attribs["url"],
        "tasks": get_all_task_ids_from_mr(event),
    }

    if attribs["author_id"] == event["user"]["id"]:
        owner = event["user"]
    else:
        owner = await gitlab_client.user_info(attribs["author_id"])
    data["owner"] = owner["username"]

    return data


def get_all_task_ids_from_mr(event):
    """Get task ids from an MR's title, description, and last commit."""
    attribs = event.get("object_attributes")
    if attribs is None:
        return set()

    def collect_ids(s):
        return set(RE_TASK_IDS.findall(s))

    description = attribs.get("description") or ""
    last_commit = attribs.get("last_commit") or ""
    if last_commit:
        last_commit = last_commit.get("message") or ""

    return collect_ids(description).union(collect_ids(last_commit))


def main():
    """Process GitLab webhook events and enqueue IRC messages."""
    return aiorun.run(
        async_main(),
        stop_on_unhandled_errors=True,
        timeout_task_shutdown=23,
    )
