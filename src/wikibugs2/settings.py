# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import pathlib

import environ

env = environ.Env()
env.smart_cast = False
environ.Env.read_env(env_file=pathlib.Path.cwd() / ".env")

PACKAGE_PATH = pathlib.Path(__file__).parent

# == IRC settings ==
IRC_SERVER = env.str("IRC_SERVER", default="irc.libera.chat")
IRC_PORT = env.int("IRC_PORT", default=6697)
IRC_USE_SSL = env.bool("IRC_USE_SSL", default=True)
IRC_USE_PASSWORD = env.bool("IRC_USE_PASSWORD", default=False)
IRC_USER = env.str("IRC_USER", default="wikibugs")
IRC_NICK = env.str("IRC_NICK", default="wikibugs")
IRC_PASSWORD = env.str("IRC_PASSWORD")
IRC_AUTOJOINS = env.list("IRC_AUTOJOINS", default=["#wikimedia-cloud"])
IRC_SAL_CHANNEL = env.str("IRC_SAL_CHANNEL", default="#wikimedia-cloud")
IRC_SAL_PROJECT = env.str("IRC_SAL_PROJECT", default="tools.wikibugs")
IRC_CHANNELS_FILE = env.str(
    "IRC_CHANNELS_FILE",
    default=PACKAGE_PATH / "channels.yaml",
)

# == Phorge settings ==
INVALID_PHAB_TOKEN = "api-badcafe012345678901234567890"
PHAB_HOST = env.str("PHAB_HOST", default="https://phabricator.wikimedia.org")
PHAB_TOKEN = env.str("PHAB_TOKEN", default=INVALID_PHAB_TOKEN)

# == Redis settings ==
REDIS_HOST = env.str(
    "REDIS_HOST",
    default="redis.svc.tools.eqiad1.wikimedia.cloud",
)
REDIS_QUEUE_NAME = env.str("REDIS_QUEUE_NAME", default="wikibugs2")

# == Event settings ==
EVENT_SERVER_URL = env.str("EVENT_SERVER_URL", default="http://wikibugs:8000")

# == Gerrit settings ==
GERRIT_SERVER = env.str("GERRIT_SERVER", default="gerrit.wikimedia.org")
GERRIT_PORT = env.int("GERRIT_PORT", default=29418)
GERRIT_SSH_USER = env.str("GERRIT_SSH_USER", default="suchabot")
GERRIT_SSH_KEY = env.str("GERRIT_SSH_KEY", multiline=True)
GERRIT_CHANNELS_FILE = env.str(
    "GERRIT_CHANNELS_FILE",
    default=PACKAGE_PATH / "gerrit-channels.yaml",
)

# == Web settings ==
WEB_BIND = env.list("WEB_BIND", default=["0.0.0.0:8000"])
WEB_GERRIT_CHANNELS_URL = env.str(
    "WEB_GERRIT_CHANNELS_URL",
    default="https://gitlab.wikimedia.org/toolforge-repos/wikibugs2/-/raw/main/src/wikibugs2/gerrit-channels.yaml",
)
WEB_GITLAB_CHANNELS_URL = env.str(
    "WEB_GITLAB_CHANNELS_URL",
    default="https://gitlab.wikimedia.org/toolforge-repos/wikibugs2/-/raw/main/src/wikibugs2/gitlab-channels.yaml",
)
WEB_PHORGE_CHANNELS_URL = env.str(
    "WEB_PHORGE_CHANNELS_URL",
    default="https://gitlab.wikimedia.org/toolforge-repos/wikibugs2/-/raw/main/src/wikibugs2/channels.yaml",
)
WEB_BEARER_TOKEN = env.str("WEB_BEARER_TOKEN")

# == GitLab settings ==
GITLAB_CHANNELS_FILE = env.str(
    "GITLAB_CHANNELS_FILE",
    default=PACKAGE_PATH / "gitlab-channels.yaml",
)
GITLAB_EVENTS_URL = env.str(
    "GITLAB_EVENTS_URL",
    default="https://gitlab-webhooks.toolforge.org/sse/",
)
GITLAB_HOST = env.str("GITLAB_HOST", default="https://gitlab.wikimedia.org")
GITLAB_TOKEN = env.str("GITLAB_TOKEN")

# == Health check settings ==
HEALTH_CHECK_FILE = env.str("HEALTH_CHECK_FILE", default="/tmp/healthz")
