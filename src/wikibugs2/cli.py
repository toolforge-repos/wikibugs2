# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import logging
import logging.handlers
import queue

import click
import coloredlogs

from .gerrit import main as gerrit_main
from .gitlab import main as gitlab_main
from .irc import main as irc_main
from .phorge import main as phorge_main
from .tools.update_credits import main as update_credits_main
from .utils import check_healthz
from .version import __version__
from .wblogging import LocalQueueHandler
from .wblogging import PrivateTimedRotatingFileHandler
from .web import main as web_main

logger = logging.getLogger(__name__)


@click.group()
@click.version_option(version=__version__)
@click.option(
    "-v",
    "--verbose",
    count=True,
    help="Increase debug logging verbosity",
)
@click.option(
    "--logfile",
    default=None,
    type=click.Path(dir_okay=False),
    help="Log to this (rotated) log file",
)
@click.pass_context
def wikibugs2(ctx, verbose, logfile):
    """IRC announce bot for issue tracker and forge events."""
    ctx.ensure_object(dict)
    ctx.obj["VERBOSE"] = verbose
    ctx.obj["LOGFILE"] = logfile

    log_fmt = "%(asctime)s %(name)s %(levelname)s: %(message)s"
    log_datefmt = "%Y-%m-%dT%H:%M:%SZ"
    coloredlogs.install(
        level=max(logging.DEBUG, logging.WARNING - (10 * verbose)),
        fmt=log_fmt,
        datefmt=log_datefmt,
        level_styles=coloredlogs.DEFAULT_LEVEL_STYLES
        | {
            "debug": {},
            "info": {"color": "green"},
        },
        field_styles=coloredlogs.DEFAULT_FIELD_STYLES
        | {
            "asctime": {"color": "yellow"},
        },
    )
    logging.captureWarnings(True)

    if logfile:
        # Write logs from a background thread
        log_queue = queue.SimpleQueue()
        queue_handler = LocalQueueHandler(log_queue)
        logging.getLogger().addHandler(queue_handler)

        handler = PrivateTimedRotatingFileHandler(
            logfile,
            when="midnight",
            backupCount=7,
            utc=True,
            encoding="utf-8",
        )
        handler.setLevel(logging.INFO)
        if verbose >= 3:
            # T359230: don't send debug level to disk until `-vvv`
            handler.setLevel(logging.DEBUG)
        handler.setFormatter(
            logging.Formatter(fmt=log_fmt, datefmt=log_datefmt),
        )

        listener = logging.handlers.QueueListener(
            log_queue,
            handler,
            respect_handler_level=True,
        )
        listener.start()
        click.echo(f"Logging to {logfile} with daily rotation.")

    logger.debug("Invoking %s", ctx.invoked_subcommand)


@wikibugs2.command()
@click.pass_context
def gerrit(ctx):
    """Process Gerrit event-stream events and enqueue IRC messages."""
    ctx.exit(gerrit_main(verbosity=ctx.obj["VERBOSE"]))


@wikibugs2.command()
@click.option(
    "--ask/--no-ask",
    default=False,
    help="Ask before pushing change to Redis",
)
@click.option(
    "--start-from",
    default=0,
    help="chronologicalKey value to start polling from.",
)
@click.argument(
    "files",
    type=click.Path(exists=True),
    nargs=-1,
)
@click.pass_context
def phorge(ctx, ask, start_from, files):
    """Process Phorge events and enqueue data for creating IRC messages.

    If files are provided, parse them as XACT files instead of polling Phorge
    """
    ctx.exit(
        phorge_main(
            ask_before_push=ask,
            start_from=start_from,
            files=files,
        ),
    )


@wikibugs2.command()
@click.pass_context
def irc(ctx):
    """Read server-sent events, format them, and send them to irc."""
    ctx.exit(irc_main())


@wikibugs2.command()
@click.pass_context
def update_credits(ctx):
    """Update CREDITS file with current contributors."""
    ctx.exit(update_credits_main())


@wikibugs2.command()
@click.pass_context
def web(ctx):
    """Multi-purpose web service."""
    ctx.exit(web_main())


@wikibugs2.command()
@click.pass_context
def gitlab(ctx):
    """Process GitLab webhook events and enqueue IRC messages."""
    ctx.exit(gitlab_main())


@wikibugs2.command()
@click.option(
    "-l",
    "--max-lag",
    default=89,
    help="Maximum timestamp lag to allow",
)
@click.pass_context
def healthz(ctx, max_lag):
    """Check for a recent timestamp in HEALTH_CHECK_FILE."""
    if not check_healthz(max_lag):
        ctx.exit(1)
    ctx.exit(0)


if __name__ == "__main__":  # pragma: nocover
    wikibugs2()
