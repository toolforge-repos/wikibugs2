# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
import logging
import socket

import irc3

from . import channelfilter
from . import events
from . import messagebuilder
from . import settings
from . import utils

current_host = socket.getfqdn()
logger = logging.getLogger(__name__)


class IrcBot(irc3.IrcBot):
    logging_config = {"version": 1, "incremental": True}


@irc3.plugin
class Queue2Irc:
    """Irc3 plugin to pull messages from a central queue."""

    def __init__(self, bot):
        self.bot = bot
        self.builder = messagebuilder.PhorgeMessageBuilder()
        self.task = None
        self.failures = 0
        self.max_failures = 10
        self.joined_channels = set()
        self.ignored_users = (
            "CodeReviewBot",  # T364575
            "CommunityTechBot",
            "gerritbot",
            "Maintenance_bot",
            "Phabricator_maintenance",
            "ReleaseTaggerBot",
            "Stashbot",
        )

    def server_ready(self):
        """Start queue to IRC pump coroutine."""
        logger.info("Queue2Irc.server_ready() starting new async task")
        self.task = self.bot.create_task(self.run())
        self.task.add_done_callback(self.handle_task_done)

    def handle_task_done(self, task):
        """React to our task ending."""
        try:
            task.result()
        except asyncio.exceptions.CancelledError:
            logger.exception("Queue2Irc task cancelled.")
            self.task = None
            # Assume that something is in charge of shutting the bot down
        except Exception:
            logger.exception("Exception raised by Queue2Irc task.")
            self.task = self.bot.create_task(self.restart_or_halt())

    async def restart_or_halt(self):
        """Decide to either restart the polling loop or kill the bot."""
        self.failures += 1
        logger.info("Consecutive failures seen: %d", self.failures)
        if self.failures < self.max_failures:
            self.task = None
            backoff = min(0.11 * 2**self.failures, 17)
            logger.info("Waiting %.3f seconds before restart", backoff)
            await asyncio.sleep(backoff)
            self.server_ready()
        else:
            logger.fatal("Shutting down the bot's loop due to failure storm.")
            asyncio.get_running_loop().stop()

    async def privmsg_many(self, channels, message):
        """Send a message to many channels."""
        for channel in channels:
            await self.privmsg(channel, message)

    async def join_channel(self, channel):
        """Join a channel."""
        if channel in self.joined_channels:
            return
        logger.info("Joining channel %s", channel)
        # irc3.IrcBot's join() doesn't return a future
        await self.bot.send_line(f"JOIN {channel}")
        self.joined_channels.add(channel)

    async def privmsg(self, target, message):
        await self.join_channel(target)
        await self.bot.privmsg(target, message, nowait=False)

    async def wait_for_protocol(self):
        """Wait for the bot to be connected."""
        logger.debug("Queue2Irc.wait_for_protocol()")
        while True:
            if getattr(self.bot, "protocol", None):
                break
            else:
                await asyncio.sleep(0.1)

    async def run(self):
        """Read messages from Queue, format them, and send them to irc."""
        await self.wait_for_protocol()
        logger.debug("Starting Queue->IRC message pump...")

        self.chanfilter = await channelfilter.ChannelFilter.create(
            origin=settings.IRC_CHANNELS_FILE,
        )

        for channel in settings.IRC_AUTOJOINS:
            await self.join_channel(channel)
        await self.join_channel(self.chanfilter.default_channel)
        await self.join_channel(self.chanfilter.firehose_channel)

        if self.failures > 0:
            logger.info("Resetting Queue2Irc failure counter")
            self.failures = 0

        logger.debug("Connecting to %s", settings.EVENT_SERVER_URL)
        async with utils.create_aiohttp_client_session() as session:
            async with events.SSEClient(
                f"{settings.EVENT_SERVER_URL}/api/event/stream",
                settings.WEB_BEARER_TOKEN,
                session,
            ) as reader:
                try:
                    async for event in reader:
                        await utils.touch_healthz_file()
                        if event.event == "irc":
                            logger.debug(
                                "Sending message to %(channels)s",
                                event.data,
                            )
                            await self.privmsg_many(**event.data)

                        elif event.event == "phorge":
                            # Create and send a Phorge message.
                            await self.handle_phorge_data(event.data)

                        elif event.event == "ping":
                            logger.info("Received ping from server. pong.")

                        else:
                            logger.warning("Unexpected event: %r", event)

                except Exception:
                    logger.exception("Oops, what's this?")
                    raise

    async def handle_phorge_data(self, data):
        if data["user"] in self.ignored_users:
            # Ignore some Phabricator bots
            logger.info("Skipped %(url)s by %(user)s", data)
            return

        updated = await self.chanfilter.update()
        if updated:
            project = settings.IRC_SAL_PROJECT
            await self.privmsg(
                settings.IRC_SAL_CHANNEL,
                f"!log {project} Updated channels.yaml to: {updated}",
            )
            logger.info("Updated channels.yaml to: %s", updated)

        channels = self.chanfilter.channels_for(
            {project: None for project in data["projects"]},
        )
        for chan, matched_projects in channels.items():
            data["channel"] = chan
            data["matched_projects"] = matched_projects
            text = self.builder.build_message(data)
            await self.privmsg(chan, text)


def main():
    """Read messages from Queue, format them, and send them to irc."""
    plugins = [
        "irc3.plugins.core",
        "irc3.plugins.ctcp",
        "irc3.plugins.log",
        __name__,  # load plugins from this module
    ]
    # SASL auth is preferred when connecting directly to an IRC network, but
    # when connecting via a BNC service password auth may be required.
    password = None
    sasl_username = settings.IRC_USER
    sasl_password = settings.IRC_PASSWORD
    if settings.IRC_USE_PASSWORD:
        password = settings.IRC_PASSWORD
        sasl_username = None
        sasl_password = None
        logger.info("Configured for password auth to %s", settings.IRC_SERVER)
    else:
        plugins.append("irc3.plugins.sasl")
        logger.info("Configured for SASL auth as %s", sasl_username)

    bot = IrcBot(
        host=settings.IRC_SERVER,
        port=settings.IRC_PORT,
        ssl=settings.IRC_USE_SSL,
        username=settings.IRC_USER,
        password=password,
        sasl_username=sasl_username,
        sasl_password=sasl_password,
        realname=(
            "Wikibugs v2.1, <https://www.mediawiki.org/wiki/Wikibugs> "
            + "running on "
            + current_host
        ),
        nick=settings.IRC_NICK,
        includes=plugins,
        ctcp={
            "finger": "{realname}",
            "source": "{realname}",
            "version": "{realname}",
            "userinfo": "{realname}",
            "ping": "PONG",
        },
    )
    return bot.run(forever=True)
