# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
import datetime
import functools
import logging
import secrets
import time

import aiorun
import hypercorn
import quart
import quart_schema
import werkzeug.exceptions

from . import metrics
from . import models
from . import settings
from . import utils
from . import wblogging
from .version import __version__

logger = logging.getLogger(__name__)

app = quart.Quart(__name__)
app.config.from_prefixed_env(prefix="WEB")
app.config.update(
    SESSION_COOKIE_HTTPONLY=True,
    SESSION_COOKIE_SAMESITE="Strict",
    SESSION_REFRESH_EACH_REQUEST=False,
)
if not app.secret_key:
    # Can be configured durably with a WEB_SECRET_KEY envvar
    app.secret_key = secrets.token_urlsafe(48)

quart_schema.QuartSchema(
    app,
    openapi_path="/openapi.json",
    redoc_ui_path=None,
    scalar_ui_path=None,
    swagger_ui_path=None,
    info={"title": "Wikibugs2", "version": __version__},
    security_schemes={"token": {"type": "http", "scheme": "bearer"}},
)


metrics.add_middleware(app)


@app.before_serving
async def load_channelfilter_config():
    """Load Gerrit and Phorge channelfilter config."""
    app.GERRIT_CONFIG = await utils.fetch_channel_config(
        settings.WEB_GERRIT_CHANNELS_URL,
    )
    app.PHORGE_CONFIG = await utils.fetch_channel_config(
        settings.WEB_PHORGE_CHANNELS_URL,
    )
    app.GITLAB_CONFIG = await utils.fetch_channel_config(
        settings.WEB_GITLAB_CHANNELS_URL,
    )


@app.before_serving
async def initialize_event_queue():
    """Setup the irc message input queue."""
    logger.debug("Creating event_queue")
    app.event_queue = asyncio.Queue()
    await asyncio.sleep(0.1)


@app.context_processor
def inject_version():
    return {"version": __version__}


@app.template_filter()
def format_time(value, fmt="%Y-%m-%d %H:%M:%S"):
    return time.strftime(fmt, time.gmtime(value))


@app.template_filter()
def human_time(value):
    now = datetime.datetime.now()
    diff = now - datetime.datetime.fromtimestamp(value)

    if diff.days == 0:
        if diff.seconds < 10:
            return "just now"
        if diff.seconds < 60:
            return f"{int(diff.seconds)} seconds ago"
        if diff.seconds < 120:
            return "a minute ago"
        if diff.seconds < 3600:
            return f"{int(diff.seconds / 60)} minutes ago"
        if diff.seconds < 7200:
            return "an hour ago"
        if diff.seconds < 86400:
            return f"{int(diff.seconds / 3600)} hours ago"
    if diff.days == 1:
        return "a day ago"
    if diff.days < 7:
        return f"{diff.days} days ago"
    if diff.days < 31:
        return f"{int(diff.days / 7)} weeks ago"
    if diff.days < 365:
        return f"{int(diff.days / 30)} months ago"
    return f"{int(diff.days / 365)} years ago"


@app.get("/")
@quart_schema.hide
async def home():
    ctx = {
        "last_seen": metrics.LAST_SEEN_TIMES,
        "boot_time": metrics.BOOT_TIME,
    }
    return await quart.render_template("index.html", **ctx)


@app.get("/healthz")
@quart_schema.hide
async def healthz():
    """Trivial 'is this process alive' check."""
    return {"status": "OK"}, 200


@app.get("/openapi-client/")
@quart_schema.hide
async def openapi_client():
    """View and use OpenAPI spec."""
    return await quart.render_template("openapi-client.html")


@app.get("/api/config/gerrit")
@quart_schema.tag(["config"])
@quart_schema.validate_response(models.ChannelFilterConfig, 200)
@quart_schema.validate_response(models.ApiError, 400)
async def api_config_gerrit():
    """Get current Gerrit channelfilter config."""
    return app.GERRIT_CONFIG, 200


@app.get("/api/config/phorge")
@quart_schema.tag(["config"])
@quart_schema.validate_response(models.ChannelFilterConfig, 200)
@quart_schema.validate_response(models.ApiError, 400)
async def api_config_phorge():
    """Get current Phorge channelfilter config."""
    return app.PHORGE_CONFIG, 200


@app.get("/api/config/gitlab")
@quart_schema.tag(["config"])
@quart_schema.validate_response(models.ChannelFilterConfig, 200)
@quart_schema.validate_response(models.ApiError, 400)
async def api_config_gitlab():
    """Get current GitLab channelfilter config."""
    return app.GITLAB_CONFIG, 200


def token_auth_required():
    """Decorator to restrict access to a known Bearer token."""

    def decorator(func):
        @quart_schema.validate_response(models.MissingToken, 401)
        @quart_schema.validate_response(models.InvalidToken, 403)
        @quart_schema.security_scheme([{"token": []}])
        @functools.wraps(func)
        async def wrapper(*args, **kwargs):
            if "Authorization" not in quart.request.headers:
                raise werkzeug.exceptions.Unauthorized(
                    "Authorization header required",
                )
            authorization = quart.request.headers["Authorization"]
            if authorization != f"Bearer {settings.WEB_BEARER_TOKEN}":
                raise werkzeug.exceptions.Forbidden(
                    "Invalid authorization token",
                )
            return await quart.current_app.ensure_async(func)(*args, **kwargs)

        return wrapper

    return decorator


@app.post("/api/config/refresh")
@quart_schema.tag(["config"])
@quart_schema.validate_response(models.EmptyBody, 204)
@token_auth_required()
async def api_config_refresh():
    """Request that config be refreshed from origin."""
    metrics.record_last_seen("config/refresh")
    await load_channelfilter_config()
    return {}, 204


@app.put("/api/event/irc")
@quart_schema.tag(["events"])
@quart_schema.validate_request(models.IrcMessage)
@quart_schema.validate_response(models.EmptyBody, 202)
@token_auth_required()
async def irc_event(data):
    """Submit a new irc message event for processing."""
    logger.info("Received irc event: %s", data)
    metrics.record_last_seen("event/irc")
    sse = models.ServerSentEvent(event="irc", data=data)
    await app.event_queue.put(sse)
    return {}, 202


@app.put("/api/event/phorge")
@quart_schema.tag(["events"])
@quart_schema.validate_request(models.PhorgeEventMetadata)
@quart_schema.validate_response(models.EmptyBody, 202)
@token_auth_required()
async def phorge_event(data):
    """Submit a new Phorge event for processing."""
    logger.info("Received phorge event: %s", data)
    metrics.record_last_seen("event/phorge")
    sse = models.ServerSentEvent(event="phorge", data=data)
    await app.event_queue.put(sse)
    return {}, 202


@app.get("/api/event/stream")
@quart_schema.tag(["events"])
@token_auth_required()
async def eventstream():
    """Connect to a text/event-stream feed."""
    if "text/event-stream" not in quart.request.accept_mimetypes:
        return {"errors": ["only available as text/event-stream"]}, 406

    async def event_queue_iterator():
        """Emit SSE encoded records for event_queue contents."""
        metrics.REQUESTS_IN_PROGRESS.inc({"path": "/api/event/stream"})
        loop = asyncio.get_running_loop()
        packet = models.ServerSentEvent(
            event="ping",
            comment="Initial connect ping",
            data={"ping": loop.time()},
        )
        logger.info("Sending initial ping: %s", packet)
        metrics.EVENTS_SENT.inc({"type": packet.event or "event"})
        yield packet.encode()

        while True:
            try:
                async with asyncio.timeout(61):
                    packet = await app.event_queue.get()
            except TimeoutError:
                packet = models.ServerSentEvent(
                    event="ping",
                    comment="Queue get timeout ping",
                    data={"ping": loop.time()},
                )
            logger.info("Sending event: %s", packet)
            metrics.EVENTS_SENT.inc({"type": packet.event or "event"})
            yield packet.encode()
        metrics.REQUESTS_IN_PROGRESS.dec({"path": "/api/event/stream"})

    response = await quart.make_response(
        event_queue_iterator(),
        {
            "Content-Type": "text/event-stream",
            "Cache-Control": "no-cache",
            "Transfer-Encoding": "chunked",
        },
    )
    response.timeout = None
    return response


@app.errorhandler(quart_schema.RequestSchemaValidationError)
async def handle_request_validation_error(error):
    return (
        models.ApiError(errors=[str(error.validation_error)]).to_dict(),
        400,
    )


@app.errorhandler(werkzeug.exceptions.Unauthorized)
async def handle_unauthorized_error(error):
    return models.MissingToken(error=error.description).to_dict(), 401


@app.errorhandler(werkzeug.exceptions.Forbidden)
async def handle_forbidden_error(error):
    return models.InvalidToken(error=error.description).to_dict(), 403


async def async_main():
    """Multi-purpose web service."""
    logger.info("Starting webservice")
    config = hypercorn.config.Config.from_mapping(
        bind=settings.WEB_BIND,
        graceful_timeout=7,
        include_server_header=False,
        logger_class=wblogging.HypercornLogger,
        use_reloader=False,
        websocket_ping_interval=5,
        worker_class="asyncio",
        workers=1,
    )
    await hypercorn.asyncio.serve(
        app,
        config,
        mode="asgi",
        shutdown_trigger=lambda: asyncio.Future(),
    )


def main():
    """Multi-purpose web service."""
    return aiorun.run(
        async_main(),
        stop_on_unhandled_errors=True,
        timeout_task_shutdown=23,
    )
