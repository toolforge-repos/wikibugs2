Wikibugs2
=========

Wikibugs2 is an IRC announce bot for bug tracker and code review events.

The bot polls the phabricator API, listens to the Gerrit events feed, and posts changes to tasks and patches into various IRC channels.

Further documentation, including deployment instructions, can be found on [the
"wikibugs" page on mediawiki.org][Wikibugs].

License
-------
Licensed under the [GPL-3.0-or-later][] license. See [COPYING][] for the full
license.

[Wikibugs]: https://www.mediawiki.org/wiki/Wikibugs
[GPL-3.0-or-later]: https://www.gnu.org/licenses/gpl-3.0.en.html
[COPYING]: https://gitlab.wikimedia.org/toolforge-repos/wikibugs2/-/blob/main/COPYING
