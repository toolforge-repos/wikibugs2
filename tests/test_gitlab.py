# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import json

import aioresponses
import pytest

import wikibugs2.gitlab
import wikibugs2.settings
import wikibugs2.utils

from . import root

data_path = root / "tests" / "data" / "gitlab_events"


@pytest.fixture
async def gitlab_client():
    async with wikibugs2.utils.create_aiohttp_client_session() as session:
        client = wikibugs2.gitlab.Client(
            wikibugs2.settings.GITLAB_HOST,
            wikibugs2.settings.GITLAB_TOKEN,
            session,
        )
        yield client


async def test_process_mr(gitlab_client):
    event = json.load(
        (data_path / "mr_update_nonowner.json").open(encoding="utf-8"),
    )
    with aioresponses.aioresponses() as m:
        m.get(
            f"{wikibugs2.settings.GITLAB_HOST}/api/v4/users/14",
            payload={"id": 14, "username": "bd808"},
        )
        data = await wikibugs2.gitlab.process_mr(event, gitlab_client)
    assert data == {
        "action": "update",
        "user": "valhallasw",
        "message": "Setup GitLab CI and expand tests that run automatically",
        "repo": "toolforge-repos/wikibugs2",
        "branch": "main",
        "url": "https://gitlab.wikimedia.org/toolforge-repos/wikibugs2/-/merge_requests/2",
        "tasks": {"T357850"},
        "owner": "bd808",
    }

    event = json.load(
        (data_path / "nonetype-has-no-attribute-get.json").open(
            encoding="utf-8",
        ),
    )
    data = await wikibugs2.gitlab.process_mr(event, gitlab_client)
    assert data == {
        "action": "open",
        "branch": "main",
        "message": "buildkit.Compile: Fix comment",
        "owner": "dancy",
        "repo": "repos/releng/blubber",
        "tasks": set(),
        "url": "https://gitlab.wikimedia.org/repos/releng/blubber/-/merge_requests/79",
        "user": "dancy",
    }
