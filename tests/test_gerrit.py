# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import pytest

import wikibugs2.gerrit
from wikibugs2.gerrit import IncludeOwner


@pytest.fixture
def event():
    return {
        "uploader": {"name": "UserName"},
        "change": {
            "project": "test",
            "branch": "master",
            "subject": "test",
            "number": "2001",
            "url": "https://gerrit.git.wmflabs.org/r/2001",
            "commitMessage": "test\n\nChange-Id: I5a0210ada1104a378c2ecbc1dc7ec6c683d0eccd\n",
            "owner": {"name": "UserName"},
        },
    }


def test_basic_full(event):
    result = wikibugs2.gerrit.process_simple(event, "test_event", "uploader")
    assert result["type"] == "test_event"
    assert result["message"] == "test"
    assert result["repo"] == "test"
    assert result["branch"] == "master"
    assert result["url"] == "https://gerrit.wikimedia.org/r/2001"
    assert result["user"] == "UserName"
    assert result["owner"] == "UserName"
    assert result["task"] is None


def test_bug_url(event):
    """Test that the _first_ included __Bug__ is included in the message"""
    event["change"][
        "commitMessage"
    ] = """test

Task: T12344
Bug: T12345
Bug: T12346
Change-Id: I5a0210ada1104a378c2ecbc1dc7ec6c683d0eccd
"""
    result = wikibugs2.gerrit.process_simple(event, "test_event", "uploader")

    assert result["task"] == "T12345"


def test_hide_if_user_is_owner(event):
    result = wikibugs2.gerrit.process_simple(
        event,
        "test_event",
        "uploader",
        IncludeOwner.IF_NOT_USER,
    )
    assert "owner" not in result


def test_include_owner_if_different(event):
    event["uploader"]["name"] = "OtherUser"
    result = wikibugs2.gerrit.process_simple(
        event,
        "test_event",
        "uploader",
        IncludeOwner.IF_NOT_USER,
    )
    assert result["user"] == "OtherUser"
    assert result["owner"] == "UserName"
