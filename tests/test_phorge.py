# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
import json

import pytest

from wikibugs2 import settings
from wikibugs2 import utils
from wikibugs2.phorge import PhorgeFeedReader

from . import root


class MockWriter:
    def __init__(self):
        self.events = []

    async def phorge_event(self, data):
        self.events.append(data)
        await asyncio.sleep(0.001)


@pytest.fixture
async def feed_reader():
    async with utils.create_aiohttp_client_session() as session:
        writer = MockWriter()
        reader = PhorgeFeedReader(session=session, writer=writer)
        yield reader


async def _process(feed_reader, event):
    event = json.load(
        (root / "tests" / "data" / "phab_events" / event).open(),
    )
    await feed_reader.process_event(event)
    return feed_reader.writer.events


@pytest.mark.skipif(
    settings.INVALID_PHAB_TOKEN == settings.PHAB_TOKEN,
    reason="Valid PHAB_TOKEN needed to query Conduit endpoints.",
)
async def test_get_tags(feed_reader):
    tasks_data = root / "tests" / "data" / "phab_tasks"
    task = json.load((tasks_data / "T87834.json").open())
    tags = await feed_reader.get_tags(task)

    assert len(tags) > 0
    assert {
        "Wikimedia-Fundraising",
        "Fundraising-Backlog-Old",
        "Wikimedia-Fundraising-CiviCRM",
        "§ Fundraising Sprint Devo",
        "Fr-tech-archived-from-FY-2014/15",
    } == tags.keys()

    assert {"shade", "disabled", "uri", "tagtype"} == tags[
        "§ Fundraising Sprint Devo"
    ].keys()
    assert tags["§ Fundraising Sprint Devo"]["shade"] == "disabled"
    assert tags["§ Fundraising Sprint Devo"]["disabled"]
    assert tags["§ Fundraising Sprint Devo"]["uri"] == (
        "https://phabricator.wikimedia.org/tag/§_fundraising_sprint_devo/"
    )
    assert tags["§ Fundraising Sprint Devo"]["tagtype"] == "calendar"

    n_disabled = 0
    n_briefcase = 0
    n_calendar = 0

    for _tag, props in tags.items():
        if props["disabled"]:
            n_disabled += 1
        if props["tagtype"] == "briefcase":
            n_briefcase += 1
        if props["tagtype"] == "calendar":
            n_calendar += 1

    assert n_disabled > 0
    assert n_briefcase > 0
    assert n_calendar > 0


@pytest.mark.skipif(
    settings.INVALID_PHAB_TOKEN == settings.PHAB_TOKEN,
    reason="Valid PHAB_TOKEN needed to query Conduit endpoints.",
)
async def test_get_tags_with_milestones(feed_reader):
    # T358653: wikibugs only shows milestone name without parent project name
    tasks_data = root / "tests" / "data" / "phab_tasks"
    task = json.load((tasks_data / "T341490.json").open())
    tags = await feed_reader.get_tags(task)

    assert len(tags) > 0
    assert {
        "Web Team Visual Regression Framework",
        "Notifications (Echo)",
        "Web-Team-Backlog (Web Team FY2023-24 Q1 Sprint 1)",
        "Growth-Team",
        "Design-System-Team",
        "MW-1.41-notes (1.41.0-wmf.17; 2023-07-11)",
    } == tags.keys()


@pytest.mark.skipif(
    settings.INVALID_PHAB_TOKEN == settings.PHAB_TOKEN,
    reason="Valid PHAB_TOKEN needed to query Conduit endpoints.",
)
async def test_add_token(feed_reader):
    messages = await _process(feed_reader, "token.json")
    assert len(messages) == 0


@pytest.mark.skipif(
    settings.INVALID_PHAB_TOKEN == settings.PHAB_TOKEN,
    reason="Valid PHAB_TOKEN needed to query Conduit endpoints.",
)
async def test_add_project(feed_reader):
    messages = await _process(feed_reader, "addproject.json")
    assert len(messages) == 1
    message = messages[0]

    assert (
        message["url"] == "https://phabricator.wikimedia.org/T163142#5040723"
    )
    assert "comment" not in message


@pytest.mark.skipif(
    settings.INVALID_PHAB_TOKEN == settings.PHAB_TOKEN,
    reason="Valid PHAB_TOKEN needed to query Conduit endpoints.",
)
async def test_many_changes(feed_reader):
    messages = await _process(feed_reader, "manychanges.json")
    assert len(messages) == 1
    message = messages[0]

    assert "priority" in message
    assert "old" in message["priority"]
    assert "new" in message["priority"]
    assert (
        message["url"] == "https://phabricator.wikimedia.org/T218788#5041358"
    )


@pytest.mark.skipif(
    settings.INVALID_PHAB_TOKEN == settings.PHAB_TOKEN,
    reason="Valid PHAB_TOKEN needed to query Conduit endpoints.",
)
async def test_new_task(feed_reader):
    messages = await _process(feed_reader, "createtask.json")
    assert len(messages) == 1
    message = messages[0]

    # A creation event's URL should not have a fragment
    assert message["url"] == "https://phabricator.wikimedia.org/T218800"


@pytest.mark.skipif(
    settings.INVALID_PHAB_TOKEN == settings.PHAB_TOKEN,
    reason="Valid PHAB_TOKEN needed to query Conduit endpoints.",
)
async def test_subtask_closed_ignored(feed_reader):
    messages = await _process(feed_reader, "subtaskclosed.json")
    assert len(messages) == 0


@pytest.mark.skipif(
    settings.INVALID_PHAB_TOKEN == settings.PHAB_TOKEN,
    reason="Valid PHAB_TOKEN needed to query Conduit endpoints.",
)
async def test_security_default_ignored(feed_reader):
    messages = await _process(feed_reader, "securitydefault.json")
    assert len(messages) == 0
