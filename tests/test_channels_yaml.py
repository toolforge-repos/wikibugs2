# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import wikibugs2.channelfilter
import wikibugs2.settings


async def test_phorge_config():
    chanfilter = await wikibugs2.channelfilter.ChannelFilter.create(
        origin=wikibugs2.settings.IRC_CHANNELS_FILE,
    )
    # An exception would have been raised if that wasn't the case
    print(f"{chanfilter.origin} has valid syntax")

    assert {"#mediawiki-feed", "#wikimedia-releng"} == set(
        chanfilter.channels_for({"Release-Engineering-Team": None}),
    )
    assert {"#mediawiki-feed", "#wikimedia-dev", "#wikimedia-releng"} == set(
        chanfilter.channels_for({"Phabricator": None}),
    )
    assert {
        "#mediawiki-feed",
        "#wikimedia-collaboration",
        "#pywikibot",
    } == set(chanfilter.channels_for({"Pywikibot-Flow": None}))


async def test_gerrit_config():
    chanfilter = await wikibugs2.channelfilter.ChannelFilter.create(
        origin=wikibugs2.settings.GERRIT_CHANNELS_FILE,
    )
    # An exception would have been raised if that wasn't the case
    print(f"{chanfilter.origin} has valid syntax")

    assert "#wikimedia-dev" == chanfilter.default_channel
    assert "#mediawiki-feed" == chanfilter.firehose_channel
