# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import pytest

from wikibugs2.channelfilter import branch_selector
from wikibugs2.channelfilter import ChannelFilter
import wikibugs2.gerrit
import wikibugs2.settings


@pytest.fixture
async def chanfilter():
    cf = await ChannelFilter.create(
        origin=wikibugs2.settings.GERRIT_CHANNELS_FILE,
    )
    return cf


async def verify(chanfilter, project, branch, channels, *, not_in=None):
    """Verifies the change is reported to at least the channels passed and the
    firehose channel, and not in not_channels.

    This limits the effects of additional reporting channels added to the yaml
    file on the tests.
    """
    firehose_channel = "#mediawiki-feed"

    report_channels = set(
        chanfilter.channels_for(
            {project: {"branch": branch}},
            branch_selector,
        ).keys(),
    )
    assert report_channels >= (channels | {firehose_channel})
    if not_in is not None:
        assert (report_channels & not_in) == set()


async def test_pywikibot_channels(chanfilter):
    await verify(chanfilter, "pywikibot/core", "master", {"#pywikibot"})
    await verify(
        chanfilter,
        "pywikibot/core",
        "refs/meta/config",
        {"#pywikibot", "#wikimedia-releng"},
    )


async def test_betacluster(chanfilter):
    await verify(
        chanfilter,
        "operations/puppet",
        "master",
        {"#wikimedia-operations"},
    )
    await verify(
        chanfilter,
        "operations/puppet",
        "betacluster",
        {"#wikimedia-releng"},
        not_in={"#wikimedia-operations"},
    )

    await verify(
        chanfilter,
        "operations/debs/wikistats",
        "master",
        set(),
        not_in={"#wikimedia-operations"},
    )
    await verify(
        chanfilter,
        "operations/debs/wikistats",
        "betacluster",
        {"#wikimedia-releng"},
        not_in={"#wikimedia-operations"},
    )


async def test_default(chanfilter):
    await verify(
        chanfilter,
        "non_registered_project",
        "master",
        {"#wikimedia-dev"},
    )

    # TODO: are this intended/expected?
    await verify(
        chanfilter,
        "non_registered_project",
        "refs/meta/config",
        {"#wikimedia-releng"},
        not_in={"wikimedia_dev"},
    )
    await verify(
        chanfilter,
        "non_registered_project",
        "wmf/test",
        {"#wikimedia-operations"},
        not_in={"wikimedia_dev"},
    )


async def test_mw_core(chanfilter):
    await verify(chanfilter, "mediawiki/core", "master", {"#wikimedia-dev"})
    await verify(
        chanfilter,
        "mediawiki/core",
        "fundraising",
        {"#wikimedia-dev", "#wikimedia-fundraising"},
    )
    await verify(
        chanfilter,
        "mediawiki/core",
        "fundraising/test",
        {"#wikimedia-dev", "#wikimedia-fundraising"},
    )
    await verify(
        chanfilter,
        "mediawiki/core",
        "refs/meta/config",
        {"#wikimedia-dev", "#wikimedia-releng"},
    )
    await verify(
        chanfilter,
        "mediawiki/core",
        "wmf/1.34.0-wmf.16",
        {"#wikimedia-dev", "#wikimedia-operations"},
    )


async def test_mw_extension(chanfilter):
    # TODO: is it expected that this extension is not reported in #wikimedia-dev?
    proj = "mediawiki/extensions/CentralNotice"

    await verify(
        chanfilter,
        proj,
        "master",
        {"#wikimedia-fundraising"},
        not_in={"#wikimedia-dev"},
    )
    await verify(
        chanfilter,
        proj,
        "refs/meta/config",
        {"#wikimedia-fundraising", "#wikimedia-releng"},
        not_in={"#wikimedia-dev"},
    )
    await verify(
        chanfilter,
        proj,
        "wmf/1.34.0-wmf.16",
        {"#wikimedia-fundraising", "#wikimedia-operations"},
        not_in={"#wikimedia-dev"},
    )
