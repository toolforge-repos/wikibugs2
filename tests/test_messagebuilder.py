# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import json

import wikibugs2.messagebuilder
from wikibugs2.messagebuilder import Color
from wikibugs2.messagebuilder import IRCMessageBuilder
from wikibugs2.messagebuilder import Style

from . import root

data_path = root / "tests" / "data" / "messagebuilder"


def test_colors():
    assert str(Color.WHITE) == "00"


def test_styles():
    assert str(Style.BOLD) == "\x02"


def test_format():
    bldr = IRCMessageBuilder()
    assert (
        bldr.fmt("hello", fg=Color.WHITE, bg=Color.BLACK, style=Style.BOLD)
        == "\x0300,01\x02hello\x0f"
    )
    assert bldr.fmta(
        "hello",
        fg=Color.WHITE,
        bg=Color.BLACK,
        style=Style.BOLD,
    ) == [
        Style.COLOR,
        Color.WHITE,
        ",",
        Color.BLACK,
        Style.BOLD,
        "hello",
        Style.RESET,
    ]
    assert bldr.fmt("", fg=Color.RED) == "\x0304\x0f"
    assert bldr.fmt("", bg=Color.RED) == "\x03,04\x0f"
    assert bldr.fmt("", style=Style.BOLD) == "\x02\x0f"
    assert bldr.fmt("") == ""


def test_escaping():
    builder = IRCMessageBuilder()
    assert builder.esc("bla\nfdafdas") == "bla fdafdas"
    assert builder.esc("bla\r\nfdafdas") == "bla  fdafdas"
    assert (
        builder.esc("Some ```preformatted``` text")
        == "Some `preformatted` text"
    )
    assert (
        builder.esc("Some `preformatted` text") == "Some `preformatted` text"
    )
    assert builder.esc("#page taavi") == "# page taavi"


def test_phorge_formatting():
    data = json.load((data_path / "backticks").open(encoding="utf-8"))
    data["channel"] = "##wikibugs"
    data["matched_projects"] = ["Operations"]

    builder = wikibugs2.messagebuilder.PhorgeMessageBuilder()
    built = builder.build_message(data)

    expected = (
        "\x0310Operations\x0f, \x0310Traffic\x0f, \x0310Patch-For-Review\x0f: "
        "Add ex cp-misc_codfw to text and upload - "
        "https://phabricator.wikimedia.org/T208588 (\x0310ops-monitoring-bot\x0f) "
        "Script wmf-auto-reimage was launched by ema on sarin.codfw.wmnet for hosts: "
        "` ['cp2006.codfw.wmnet', 'cp2012.codfw.wmnet'] ` The log can be found in `/var/lo..."
    )

    assert built == expected

    # T180293: Hide User-XX projects from wikibugs output
    data = json.load((data_path / "T180293.json").open(encoding="utf-8"))
    data["channel"] = "##wikibugs2"
    data["matched_projects"] = ["Wikibugs"]
    assert builder.build_message(data) == (
        "\x0310Wikibugs\x0f: "
        "Hide User-XX projects from wikibugs output - "
        "https://phabricator.wikimedia.org/T180293#9622050 (\x0310bd808\x0f) "
        "\x0305Open\x0f→\x0303In progress\x0f a:\x0303bd808\x0f "
    )

    data["matched_projects"] = ["User-bd808"]
    assert builder.build_message(data) == (
        "\x0315User-bd808\x0f, \x0310Wikibugs\x0f: "
        "Hide User-XX projects from wikibugs output - "
        "https://phabricator.wikimedia.org/T180293#9622050 (\x0310bd808\x0f) "
        "\x0305Open\x0f→\x0303In progress\x0f a:\x0303bd808\x0f "
    )

    # T90339: Use case-insensitive sort for tags added to the irc log
    data = json.load((data_path / "T90339.json").open(encoding="utf-8"))
    data["channel"] = "##wikibugs2"
    data["matched_projects"] = ["Pywikibot-General", "pywikibot-core"]
    assert builder.build_message(data) == (
        "\x0310pywikibot-core\x0f, \x0310Pywikibot-General\x0f: "
        "core Page.templates has incompatible API change from compat - "
        "https://phabricator.wikimedia.org/T58188#639703 "
        "(\x0310valhallasw\x0f) "
        "Blocker is not really the right term for this ('should be fixed "
        "before release'), and calling it 'blocker' will probably screw up "
        "WMF's weekly repo..."
    )

    # T140881: Print events in closed tasks in grey
    data = json.load((data_path / "T140881.json").open(encoding="utf-8"))
    data["channel"] = "##wikibugs2"
    data["matched_projects"] = ["Wikibugs"]
    assert builder.build_message(data) == (
        "\x0310Wikibugs\x0f: "
        "\x1ewikibugs test bug\x0f - "
        "\x1ehttps://phabricator.wikimedia.org/T1152#9622293\x0f "
        "(\x0310bd808\x0f) "
        "Comment on a closed task for {T140881}"
    )

    # T128868: Show when a task has been closed as a duplicate
    data = json.load((data_path / "T128868.json").open(encoding="utf-8"))
    data["channel"] = "##wikibugs2"
    data["matched_projects"] = []
    assert builder.build_message(data) == (
        "\x0310cloud-services-team\x0f, \x0310wikitech.wikimedia.org\x0f: "
        "\x1eInstall Extension:DynamicPageList on wikitech.wikimedia.org\x0f - "
        "\x1ehttps://phabricator.wikimedia.org/T359866#9620955\x0f "
        "(\x0310JJMC89\x0f) "
        "→\x0314Duplicate\x0f dup:\x0303T284813\x0f "
    )


def test_gerrit_formatting():
    builder = wikibugs2.messagebuilder.GerritMessageBuilder()

    data = json.load((data_path / "new_change.json").open(encoding="utf-8"))
    assert builder.build_message(data) == (
        "(\x0303PS1\x0f) "
        "\x0310\x02RealName 1\x0f: "
        "test [test] - "
        "\x0310https://gerrit.wikimedia.org/r/2001\x0f"
    )

    data = json.load((data_path / "new_patchset.json").open(encoding="utf-8"))
    assert builder.build_message(data) == (
        "(\x0303PS3\x0f) "
        "\x0310\x02RealName 1\x0f: "
        "Testing draft changes [test/gerrit-ping] - "
        "\x0310https://gerrit.wikimedia.org/r/429546\x0f"
    )

    data = json.load(
        (data_path / "new_cr_plus_2.json").open(encoding="utf-8"),
    )
    assert builder.build_message(data) == (
        "(\x0303CR\x0f) "
        "\x0310\x02RealName 1\x0f: "
        "[V:\x0303+1\x0f C:\x0303\x02+2\x0f] "
        "Refactor json-loaded tests "
        "(\x0303\x026\x0f comments) "
        "[labs/tools/wikibugs2] (work/bd808/writing-tests) - "
        "\x0310https://gerrit.wikimedia.org/r/450447\x0f "
        "(https://phabricator.wikimedia.org/T360038) "
        "(owner: \x0310\x02RealName 2\x0f)"
    )

    data = json.load((data_path / "jerkins.json").open(encoding="utf-8"))
    assert builder.build_message(data) == (
        "(\x0303CR\x0f) "
        "\x0310\x02CI reject\x0f: "
        "[V:\x0304-1\x0f] "
        "Event pager  [extensions/CampaignEvents]] - "
        "\x0310https://gerrit.wikimedia.org/r/992408\x0f "
        "(https://phabricator.wikimedia.org/T353382) "
        "(owner: \x0310\x02Mhorsey\x0f)"
    )


def test_gitlab_formatting():
    builder = wikibugs2.messagebuilder.GitLabMessageBuilder()

    data = json.load(
        (data_path / "gitlab-mr-open.json").open(encoding="utf-8"),
    )
    assert builder.build_message(data) == (
        "(\x0303open\x0f) "
        "\x0310\x02bd808\x0f: "
        "gitlab and concurrency fixes [toolforge-repos/wikibugs2] - "
        "\x0310https://gitlab.wikimedia.org/toolforge-repos/wikibugs2/-/merge_requests/37\x0f"
    )

    data = json.load(
        (data_path / "gitlab-mr-update.json").open(encoding="utf-8"),
    )
    assert builder.build_message(data) == (
        "(\x0303update\x0f) "
        "\x0310\x02valhallasw\x0f: "
        "Setup GitLab CI and expand tests that run automatically [toolforge-repos/wikibugs2] - "
        "\x0310https://gitlab.wikimedia.org/toolforge-repos/wikibugs2/-/merge_requests/2\x0f "
        "(https://phabricator.wikimedia.org/T2001 https://phabricator.wikimedia.org/T357850) "
        "(owner: \x0310\x02bd808\x0f)"
    )
