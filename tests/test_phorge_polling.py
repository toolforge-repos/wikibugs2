# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs2.
#
# Wikibugs2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs2.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
import json

import aioresponses
import pytest

from wikibugs2 import settings
from wikibugs2 import utils
import wikibugs2.phorge


def parse_request(**kwargs):
    assert kwargs["data"]["output"] == "json"
    return json.loads(kwargs["data"]["params"])


class PhorgeFixture:
    def __init__(self, feed_reader):
        self.events = []
        self.fixture = feed_reader
        self.fixture.process_event = self.process_event

    async def process_event(self, event):
        self.events.append(event)
        await asyncio.sleep(0.001)

    async def poll(self):
        await self.fixture.poll()


class MockWriter:
    def __init__(self):
        self.events = []

    async def phorge_event(self, data):
        self.events.append(data)
        await asyncio.sleep(0.001)


@pytest.fixture
async def feed_reader():
    async with utils.create_aiohttp_client_session() as session:
        writer = MockWriter()
        reader = wikibugs2.phorge.PhorgeFeedReader(
            session=session,
            writer=writer,
        )
        yield reader


@pytest.fixture
def bugs(feed_reader):
    return PhorgeFixture(feed_reader)


def feed_query_initial(url, **kwargs):  # noqa: U100 Unused argument
    content = parse_request(**kwargs)
    assert int(content["limit"]) == 1
    assert "before" not in content
    return aioresponses.CallbackResult(
        payload=json.loads(
            r"""{"result":{"PHID-STRY-cdxv7sji5d7wnjmiuqgv":{"class":"PhabricatorApplicationTransactionFeedStory","epoch":1577802875,"authorPHID":"PHID-USER-pzp7mdlx7otgdlggnyhh","chronologicalKey":"6776611750075855743","data":{"objectPHID":"PHID-TASK-rnay3rzefpqhoaqm3guo","transactionPHIDs":{"PHID-XACT-TASK-5esu7y3d7evlsi2":"PHID-XACT-TASK-5esu7y3d7evlsi2"}}}},"error_code":null,"error_info":null}""",  # noqa: E501 line too long
        ),
    )


def feed_query_second(url, **kwargs):  # noqa: U100 Unused argument
    content = parse_request(**kwargs)
    assert content["before"] == 6776611750075855743
    assert content["view"] == "data"
    return aioresponses.CallbackResult(
        payload=json.loads(
            r"""{"result":[],"error_code":null,"error_info":null}""",
        ),
    )


def feed_query_third(url, **kwargs):  # noqa: U100 Unused argument
    content = parse_request(**kwargs)
    assert content["before"] == 6776611750075855743
    assert content["view"] == "data"
    return aioresponses.CallbackResult(
        payload=json.loads(
            r"""{"result":{"PHID-STRY-etrbfg7qqflcsoexaxqr":{"class":"PhabricatorApplicationTransactionFeedStory","epoch":1577804347,"authorPHID":"PHID-USER-idceizaw6elwiwm5xshb","chronologicalKey":"6776618070283272953","data":{"objectPHID":"PHID-TASK-he2h6hqmwrdrav3cxqew","transactionPHIDs":{"PHID-XACT-TASK-k6asmqpfv2t37tp":"PHID-XACT-TASK-k6asmqpfv2t37tp"}}},"PHID-STRY-x6pr64eeimmcjl3jbsay":{"class":"PhabricatorApplicationTransactionFeedStory","epoch":1577804344,"authorPHID":"PHID-USER-idceizaw6elwiwm5xshb","chronologicalKey":"6776618060350723377","data":{"objectPHID":"PHID-TASK-he2h6hqmwrdrav3cxqew","transactionPHIDs":{"PHID-XACT-TASK-ix5urhvrpvn22e2":"PHID-XACT-TASK-ix5urhvrpvn22e2"}}},"PHID-STRY-cpcsc3r3444i3vaw66bo":{"class":"PhabricatorApplicationTransactionFeedStory","epoch":1577804267,"authorPHID":"PHID-USER-muirnivxp5hzppn2a3z7","chronologicalKey":"6776617727166200626","data":{"objectPHID":"PHID-TASK-dgq26etiz4wecd24gkmb","transactionPHIDs":{"PHID-XACT-TASK-zd6b2kmmj5pnfwm":"PHID-XACT-TASK-zd6b2kmmj5pnfwm"}}}},"error_code":null,"error_info":null}""",  # noqa: E501 line too long
        ),
    )


def feed_query_error_response(url, **kwargs):  # noqa: U100 Unused argument
    return aioresponses.CallbackResult(
        payload=json.loads(
            r"""{"result":null,"error_code":"ERR-CONDUIT-CORE","error_info":"Cursor \"6771969043218032437\" does not identify a valid object in query \"PhabricatorFeedQuery\"."}""",  # noqa: E501 line too long
        ),
    )


async def test_polling(bugs):
    with aioresponses.aioresponses() as m:
        m.post(
            f"{settings.PHAB_HOST}/api/feed.query",
            callback=feed_query_initial,
        )
        m.post(
            f"{settings.PHAB_HOST}/api/feed.query",
            callback=feed_query_second,
        )
        await bugs.poll()

        m.assert_called()
        assert bugs.events == []

    with aioresponses.aioresponses() as m:
        m.post(
            f"{settings.PHAB_HOST}/api/feed.query",
            callback=feed_query_third,
        )
        await bugs.poll()

        m.assert_called()
        assert len(bugs.events) == 3

        # TODO: add more extensive tests


async def test_error_response(bugs):
    with aioresponses.aioresponses() as m:
        m.post(
            f"{settings.PHAB_HOST}/api/feed.query",
            callback=feed_query_initial,
        )
        m.post(
            f"{settings.PHAB_HOST}/api/feed.query",
            callback=feed_query_second,
        )
        await bugs.poll()
        m.assert_called()
        assert len(bugs.events) == 0

    with aioresponses.aioresponses() as m:
        m.post(
            f"{settings.PHAB_HOST}/api/feed.query",
            callback=feed_query_error_response,
        )
        await bugs.poll()
        m.assert_called()
        assert len(bugs.events) == 0
